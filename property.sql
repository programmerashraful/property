-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 24, 2020 at 11:13 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `property`
--

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci,
  `date_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `subject`, `message`, `date_time`) VALUES
(2, 'Ripon', 'ripon@gmail.com', 'alkdsfjsldf lkdsjf', 'sdakfj lksad fjf lkadsasf sadf sdaf \r\nsadf sadf \r\nsadf \r\nsd f\r\nsadf', '2019-10-11 20:34:43');

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE `districts` (
  `id` int(2) UNSIGNED NOT NULL,
  `division_id` int(2) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `bn_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `lat` double NOT NULL,
  `lon` double NOT NULL,
  `website` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`id`, `division_id`, `name`, `bn_name`, `lat`, `lon`, `website`) VALUES
(1, 3, 'Dhaka', 'ঢাকা', 23.7115253, 90.4111451, 'www.dhaka.gov.bd'),
(2, 3, 'Faridpur', 'ফরিদপুর', 23.6070822, 89.8429406, 'www.faridpur.gov.bd'),
(3, 3, 'Gazipur', 'গাজীপুর', 24.0022858, 90.4264283, 'www.gazipur.gov.bd'),
(4, 3, 'Gopalganj', 'গোপালগঞ্জ', 23.0050857, 89.8266059, 'www.gopalganj.gov.bd'),
(5, 8, 'Jamalpur', 'জামালপুর', 24.937533, 89.937775, 'www.jamalpur.gov.bd'),
(6, 3, 'Kishoreganj', 'কিশোরগঞ্জ', 24.444937, 90.776575, 'www.kishoreganj.gov.bd'),
(7, 3, 'Madaripur', 'মাদারীপুর', 23.164102, 90.1896805, 'www.madaripur.gov.bd'),
(8, 3, 'Manikganj', 'মানিকগঞ্জ', 0, 0, 'www.manikganj.gov.bd'),
(9, 3, 'Munshiganj', 'মুন্সিগঞ্জ', 0, 0, 'www.munshiganj.gov.bd'),
(10, 8, 'Mymensingh', 'ময়মনসিংহ', 0, 0, 'www.mymensingh.gov.bd'),
(11, 3, 'Narayanganj', 'নারায়াণগঞ্জ', 23.63366, 90.496482, 'www.narayanganj.gov.bd'),
(12, 3, 'Narsingdi', 'নরসিংদী', 23.932233, 90.71541, 'www.narsingdi.gov.bd'),
(13, 8, 'Netrokona', 'নেত্রকোণা', 24.870955, 90.727887, 'www.netrokona.gov.bd'),
(14, 3, 'Rajbari', 'রাজবাড়ি', 23.7574305, 89.6444665, 'www.rajbari.gov.bd'),
(15, 3, 'Shariatpur', 'শরীয়তপুর', 0, 0, 'www.shariatpur.gov.bd'),
(16, 8, 'Sherpur', 'শেরপুর', 25.0204933, 90.0152966, 'www.sherpur.gov.bd'),
(17, 3, 'Tangail', 'টাঙ্গাইল', 0, 0, 'www.tangail.gov.bd'),
(18, 5, 'Bogura', 'বগুড়া', 24.8465228, 89.377755, 'www.bogra.gov.bd'),
(19, 5, 'Joypurhat', 'জয়পুরহাট', 0, 0, 'www.joypurhat.gov.bd'),
(20, 5, 'Naogaon', 'নওগাঁ', 0, 0, 'www.naogaon.gov.bd'),
(21, 5, 'Natore', 'নাটোর', 24.420556, 89.000282, 'www.natore.gov.bd'),
(22, 5, 'Chapainawabganj', 'চাঁপাইনবাবগঞ্জ', 24.5965034, 88.2775122, 'www.chapainawabganj.gov.bd'),
(23, 5, 'Pabna', 'পাবনা', 23.998524, 89.233645, 'www.pabna.gov.bd'),
(24, 5, 'Rajshahi', 'রাজশাহী', 0, 0, 'www.rajshahi.gov.bd'),
(25, 5, 'Sirajgonj', 'সিরাজগঞ্জ', 24.4533978, 89.7006815, 'www.sirajganj.gov.bd'),
(26, 6, 'Dinajpur', 'দিনাজপুর', 25.6217061, 88.6354504, 'www.dinajpur.gov.bd'),
(27, 6, 'Gaibandha', 'গাইবান্ধা', 25.328751, 89.528088, 'www.gaibandha.gov.bd'),
(28, 6, 'Kurigram', 'কুড়িগ্রাম', 25.805445, 89.636174, 'www.kurigram.gov.bd'),
(29, 6, 'Lalmonirhat', 'লালমনিরহাট', 0, 0, 'www.lalmonirhat.gov.bd'),
(30, 6, 'Nilphamari', 'নীলফামারী', 25.931794, 88.856006, 'www.nilphamari.gov.bd'),
(31, 6, 'Panchagarh', 'পঞ্চগড়', 26.3411, 88.5541606, 'www.panchagarh.gov.bd'),
(32, 6, 'Rangpur', 'রংপুর', 25.7558096, 89.244462, 'www.rangpur.gov.bd'),
(33, 6, 'Thakurgaon', 'ঠাকুরগাঁও', 26.0336945, 88.4616834, 'www.thakurgaon.gov.bd'),
(34, 1, 'Barguna', 'বরগুনা', 0, 0, 'www.barguna.gov.bd'),
(35, 1, 'Barishal', 'বরিশাল', 0, 0, 'www.barisal.gov.bd'),
(36, 1, 'Bhola', 'ভোলা', 22.685923, 90.648179, 'www.bhola.gov.bd'),
(37, 1, 'Jhalokati', 'ঝালকাঠি', 0, 0, 'www.jhalakathi.gov.bd'),
(38, 1, 'Patuakhali', 'পটুয়াখালী', 22.3596316, 90.3298712, 'www.patuakhali.gov.bd'),
(39, 1, 'Pirojpur', 'পিরোজপুর', 0, 0, 'www.pirojpur.gov.bd'),
(40, 2, 'Bandarban', 'বান্দরবান', 22.1953275, 92.2183773, 'www.bandarban.gov.bd'),
(41, 2, 'Brahmanbaria', 'ব্রাহ্মণবাড়িয়া', 23.9570904, 91.1119286, 'www.brahmanbaria.gov.bd'),
(42, 2, 'Chandpur', 'চাঁদপুর', 23.2332585, 90.6712912, 'www.chandpur.gov.bd'),
(43, 2, 'Chattogram', 'চট্টগ্রাম', 22.335109, 91.834073, 'www.chittagong.gov.bd'),
(44, 2, 'Cumilla', 'কুমিল্লা', 23.4682747, 91.1788135, 'www.comilla.gov.bd'),
(45, 2, 'Cox\'s Bazar', 'কক্স বাজার', 0, 0, 'www.coxsbazar.gov.bd'),
(46, 2, 'Feni', 'ফেনী', 23.023231, 91.3840844, 'www.feni.gov.bd'),
(47, 2, 'Khagrachhari', 'খাগড়াছড়ি', 23.119285, 91.984663, 'www.khagrachhari.gov.bd'),
(48, 2, 'Lakshmipur', 'লক্ষ্মীপুর', 22.942477, 90.841184, 'www.lakshmipur.gov.bd'),
(49, 2, 'Noakhali', 'নোয়াখালী', 22.869563, 91.099398, 'www.noakhali.gov.bd'),
(50, 2, 'Rangamati', 'রাঙ্গামাটি', 0, 0, 'www.rangamati.gov.bd'),
(51, 7, 'Habiganj', 'হবিগঞ্জ', 24.374945, 91.41553, 'www.habiganj.gov.bd'),
(52, 7, 'Moulvibazar', 'মৌলভীবাজার', 24.482934, 91.777417, 'www.moulvibazar.gov.bd'),
(53, 7, 'Sunamganj', 'সুনামগঞ্জ', 25.0658042, 91.3950115, 'www.sunamganj.gov.bd'),
(54, 7, 'Sylhet', 'সিলেট', 24.8897956, 91.8697894, 'www.sylhet.gov.bd'),
(55, 4, 'Bagerhat', 'বাগেরহাট', 22.651568, 89.785938, 'www.bagerhat.gov.bd'),
(56, 4, 'Chuadanga', 'চুয়াডাঙ্গা', 23.6401961, 88.841841, 'www.chuadanga.gov.bd'),
(57, 4, 'Jashore', 'যশোর', 23.16643, 89.2081126, 'www.jessore.gov.bd'),
(58, 4, 'Jhenaidah', 'ঝিনাইদহ', 23.5448176, 89.1539213, 'www.jhenaidah.gov.bd'),
(59, 4, 'Khulna', 'খুলনা', 22.815774, 89.568679, 'www.khulna.gov.bd'),
(60, 4, 'Kushtia', 'কুষ্টিয়া', 23.901258, 89.120482, 'www.kushtia.gov.bd'),
(61, 4, 'Magura', 'মাগুরা', 23.487337, 89.419956, 'www.magura.gov.bd'),
(62, 4, 'Meherpur', 'মেহেরপুর', 23.762213, 88.631821, 'www.meherpur.gov.bd'),
(63, 4, 'Narail', 'নড়াইল', 23.172534, 89.512672, 'www.narail.gov.bd'),
(64, 4, 'Satkhira', 'সাতক্ষীরা', 0, 0, 'www.satkhira.gov.bd');

-- --------------------------------------------------------

--
-- Table structure for table `divisions`
--

CREATE TABLE `divisions` (
  `id` int(2) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `bn_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `divisions`
--

INSERT INTO `divisions` (`id`, `name`, `bn_name`) VALUES
(1, 'Barishal', 'বরিশাল'),
(2, 'Chattogram', 'চট্টগ্রাম'),
(3, 'Dhaka', 'ঢাকা'),
(4, 'Khulna', 'খুলনা'),
(5, 'Rajshahi', 'রাজশাহী'),
(6, 'Rangpur', 'রংপুর'),
(7, 'Sylhet', 'সিলেট'),
(8, 'Mymensingh', 'ময়মনসিংহ');

-- --------------------------------------------------------

--
-- Table structure for table `messenger`
--

CREATE TABLE `messenger` (
  `id` int(11) NOT NULL,
  `property_id` int(50) DEFAULT NULL,
  `receive_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci DEFAULT 'new',
  `message` text COLLATE utf8_unicode_ci,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `messenger`
--

INSERT INTO `messenger` (`id`, `property_id`, `receive_id`, `sender_id`, `status`, `message`, `date`) VALUES
(1, NULL, 41, 44, 'read', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam quibusdam sit ex suscipit illo hic mollitia. Itaque expedita similique ad.zsdf', '2020-01-16 20:37:54'),
(2, NULL, 44, 41, 'read', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam quibusdam sit ex suscipit illo hic mollitia. Itaque expedita similique ad.', '2020-01-16 20:37:54'),
(3, NULL, 41, 44, 'read', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam quibusdam sit ex suscipit illo hic mollitia. Itaque expedita similique ad.', '2020-01-16 20:37:54'),
(4, NULL, 44, 41, 'read', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam quibusdam sit ex suscipit illo hic mollitia. Itaque expedita similique ad.', '2020-01-16 20:37:54'),
(5, NULL, 41, 44, 'read', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam quibusdam sit ex suscipit illo hic mollitia. Itaque expedita similique ad.', '2020-01-16 20:37:54'),
(8, NULL, 41, 44, 'read', 'protected function send_message($with){\r\n        if($this->input->post(\\\'send_message\\\')){\r\n            $attr=array(\r\n                \\\'message\\\'=>addslashes($this->input->post(\\\'message\\\')),\r\n                \\\'sender_id\\\'=>$this->user_id,\r\n                \\\'receiver_id\\\'=>$with,\r\n            );\r\n            \r\n            if($this->db->insert(\\\'messages\\\', $attr)){\r\n                \r\n            }else{\r\n                \r\n            }\r\n        }\r\n    }', '2020-01-16 23:21:10'),
(9, NULL, 43, 44, 'read', 'Test Message', '2020-01-17 00:45:17'),
(10, NULL, 41, 44, 'read', 'Another Test', '2020-01-17 00:47:15'),
(11, NULL, 44, 41, 'read', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam quibusdam sit ex suscipit illo hic mollitia. Itaque expedita similique ad.', '2020-01-17 01:09:23'),
(12, NULL, 41, 44, 'read', 'Text Another ', '2020-01-17 01:15:14'),
(13, NULL, 43, 44, 'read', 'Hello', '2020-01-17 01:16:58'),
(14, NULL, 44, 43, 'read', 'Yes I am here ', '2020-01-17 01:21:25'),
(15, NULL, 43, 44, 'read', 'Please Show me your property ', '2020-01-17 01:31:15'),
(16, NULL, 43, 44, 'read', 'http://localhost/property/users/messenger/43', '2020-01-17 01:31:23'),
(17, NULL, 43, 45, 'read', 'Tex Message test', '2020-01-17 01:33:34'),
(18, NULL, 43, 45, 'read', 'Another Text Message', '2020-01-17 01:33:52'),
(19, NULL, 44, 41, 'read', 'Already Send ', '2020-01-17 01:41:44'),
(20, NULL, 43, 30, 'read', 'Test Message', '2020-01-17 01:57:03');

-- --------------------------------------------------------

--
-- Table structure for table `properties`
--

CREATE TABLE `properties` (
  `id` int(11) NOT NULL,
  `division_id` int(50) DEFAULT NULL,
  `district_id` int(50) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bed_room` int(5) DEFAULT NULL,
  `bath_room` int(5) DEFAULT NULL,
  `dining_room` int(5) DEFAULT NULL,
  `drawing_room` int(5) DEFAULT NULL,
  `lift` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `garage` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `property_images` text COLLATE utf8_unicode_ci,
  `youtube_video` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_location` text COLLATE utf8_unicode_ci,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` int(255) DEFAULT NULL,
  `rent_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rating` int(1) DEFAULT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(255) DEFAULT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Requested'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `properties`
--

INSERT INTO `properties` (`id`, `division_id`, `district_id`, `title`, `bed_room`, `bath_room`, `dining_room`, `drawing_room`, `lift`, `garage`, `description`, `image`, `property_images`, `youtube_video`, `google_location`, `address`, `phone`, `email`, `type`, `price`, `rent_type`, `rating`, `entry_date`, `user_id`, `status`) VALUES
(1, 7, 54, 'Nice Flat Example', 3, 3, 1, 1, '1', '1', 'This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. ', 'property.jpg', NULL, 'https://www.youtube.com/embed/a9_N1RTQYg4', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14478.476066394593!2d91.808344!3d24.8768585!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3c060575911cd31d!2sLeading%20University!5e0!3m2!1sen!2sbd!4v1576483060221!5m2!1sen!2sbd\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>', 'This is property address', '01737963893', 'kimitanzina@gmail.com', 'Sale ', 500000, '', 3, '2019-12-24 04:08:40', 41, 'Requested'),
(2, 7, 54, 'Nice Flat Example', 3, 3, 1, 1, '1', '1', 'This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. ', 'property.jpg', NULL, 'https://www.youtube.com/embed/a9_N1RTQYg4', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14478.476066394593!2d91.808344!3d24.8768585!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3c060575911cd31d!2sLeading%20University!5e0!3m2!1sen!2sbd!4v1576483060221!5m2!1sen!2sbd\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>', 'This is property address', '01737963893', 'kimitanzina@gmail.com', 'Sale ', 500000, '0', 3, '2019-12-24 04:08:40', 43, 'Requested'),
(3, 7, 54, 'Nice Flat Example', 3, 3, 1, 1, '1', '1', 'This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. ', 'property.jpg', NULL, 'https://www.youtube.com/embed/a9_N1RTQYg4', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14478.476066394593!2d91.808344!3d24.8768585!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3c060575911cd31d!2sLeading%20University!5e0!3m2!1sen!2sbd!4v1576483060221!5m2!1sen!2sbd\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>', 'This is property address', '01737963893', 'kimitanzina@gmail.com', 'Rent', 500000, 'Yearly', 3, '2019-12-24 04:08:40', 41, 'Requested'),
(4, 7, 54, 'Nice Flat Example', 3, 3, 1, 1, '1', '1', 'This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. ', 'property.jpg', NULL, 'https://www.youtube.com/embed/a9_N1RTQYg4', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14478.476066394593!2d91.808344!3d24.8768585!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3c060575911cd31d!2sLeading%20University!5e0!3m2!1sen!2sbd!4v1576483060221!5m2!1sen!2sbd\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>', 'This is property address', '01737963893', 'kimitanzina@gmail.com', 'Rent', 500000, 'Monthly', 3, '2019-12-24 04:08:40', 41, 'Published'),
(5, 7, 54, 'Nice Flat Example', 3, 3, 1, 1, '1', '1', 'This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. ', 'property.jpg', NULL, 'https://www.youtube.com/embed/a9_N1RTQYg4', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14478.476066394593!2d91.808344!3d24.8768585!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3c060575911cd31d!2sLeading%20University!5e0!3m2!1sen!2sbd!4v1576483060221!5m2!1sen!2sbd\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>', 'This is property address', '01737963893', 'kimitanzina@gmail.com', 'Sale ', 500000, '0', 3, '2019-12-24 04:08:40', 41, 'Published'),
(6, 7, 54, 'Nice Flat Example', 3, 3, 1, 1, '1', '1', 'This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. ', 'property.jpg', NULL, 'https://www.youtube.com/embed/a9_N1RTQYg4', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14478.476066394593!2d91.808344!3d24.8768585!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3c060575911cd31d!2sLeading%20University!5e0!3m2!1sen!2sbd!4v1576483060221!5m2!1sen!2sbd\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>', 'This is property address', '01737963893', 'kimitanzina@gmail.com', 'Sale ', 500000, '0', 3, '2019-12-24 04:08:40', 41, 'Published'),
(7, 7, 54, 'Nice Flat Example', 3, 3, 1, 1, '1', '1', 'This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. ', 'property.jpg', NULL, 'https://www.youtube.com/embed/a9_N1RTQYg4', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14478.476066394593!2d91.808344!3d24.8768585!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3c060575911cd31d!2sLeading%20University!5e0!3m2!1sen!2sbd!4v1576483060221!5m2!1sen!2sbd\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>', 'This is property address', '01737963893', 'kimitanzina@gmail.com', 'Rent', 500000, 'Yearly', 3, '2019-12-24 04:08:40', 41, 'Published'),
(8, 7, 54, 'Nice Flat Example', 3, 3, 1, 1, '1', '1', 'This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. ', 'property.jpg', NULL, 'https://www.youtube.com/embed/a9_N1RTQYg4', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14478.476066394593!2d91.808344!3d24.8768585!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3c060575911cd31d!2sLeading%20University!5e0!3m2!1sen!2sbd!4v1576483060221!5m2!1sen!2sbd\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>', 'This is property address', '01737963893', 'kimitanzina@gmail.com', 'Rent', 500000, 'Monthly', 3, '2019-12-24 04:08:40', 41, 'Published'),
(9, 7, 54, 'Nice Flat Example', 3, 3, 1, 1, '1', '1', 'This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. ', 'property.jpg', NULL, 'https://www.youtube.com/embed/a9_N1RTQYg4', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14478.476066394593!2d91.808344!3d24.8768585!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3c060575911cd31d!2sLeading%20University!5e0!3m2!1sen!2sbd!4v1576483060221!5m2!1sen!2sbd\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>', 'This is property address', '01737963893', 'kimitanzina@gmail.com', 'Sale ', 500000, '0', 3, '2019-12-24 04:08:40', 41, 'Published'),
(10, 7, 54, 'Nice Flat Example', 3, 3, 1, 1, '1', '1', 'This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. ', 'property.jpg', NULL, 'https://www.youtube.com/embed/a9_N1RTQYg4', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14478.476066394593!2d91.808344!3d24.8768585!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3c060575911cd31d!2sLeading%20University!5e0!3m2!1sen!2sbd!4v1576483060221!5m2!1sen!2sbd\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>', 'This is property address', '01737963893', 'kimitanzina@gmail.com', 'Rent', 500000, 'Monthly', 3, '2019-12-24 04:08:40', 41, 'Published'),
(11, 7, 54, 'Nice Flat Example', 3, 3, 1, 1, '1', '1', 'This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. ', 'property.jpg', NULL, 'https://www.youtube.com/embed/a9_N1RTQYg4', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14478.476066394593!2d91.808344!3d24.8768585!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3c060575911cd31d!2sLeading%20University!5e0!3m2!1sen!2sbd!4v1576483060221!5m2!1sen!2sbd\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>', 'This is property address', '01737963893', 'kimitanzina@gmail.com', 'Sale ', 500000, '0', 3, '2019-12-24 04:08:40', 41, 'Published'),
(12, 7, 54, 'Nice Flat Example', 3, 3, 1, 1, '1', '1', 'This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. ', 'property.jpg', NULL, 'https://www.youtube.com/embed/a9_N1RTQYg4', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14478.476066394593!2d91.808344!3d24.8768585!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3c060575911cd31d!2sLeading%20University!5e0!3m2!1sen!2sbd!4v1576483060221!5m2!1sen!2sbd\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>', 'This is property address', '01737963893', 'kimitanzina@gmail.com', 'Rent', 500000, 'Monthly', 3, '2019-12-24 04:08:40', 41, 'Published'),
(13, 7, 54, 'Nice Flat Example', 3, 3, 1, 1, '1', '1', 'This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. ', 'property.jpg', NULL, 'https://www.youtube.com/embed/a9_N1RTQYg4', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14478.476066394593!2d91.808344!3d24.8768585!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3c060575911cd31d!2sLeading%20University!5e0!3m2!1sen!2sbd!4v1576483060221!5m2!1sen!2sbd\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>', 'This is property address', '01737963893', 'kimitanzina@gmail.com', 'Rent', 500000, 'Monthly', 3, '2019-12-24 04:08:40', 41, 'Published'),
(14, 7, 54, 'Nice Flat Example', 3, 3, 1, 1, '1', '1', 'This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. ', 'property.jpg', NULL, 'https://www.youtube.com/embed/a9_N1RTQYg4', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14478.476066394593!2d91.808344!3d24.8768585!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3c060575911cd31d!2sLeading%20University!5e0!3m2!1sen!2sbd!4v1576483060221!5m2!1sen!2sbd\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>', 'This is property address', '01737963893', 'kimitanzina@gmail.com', 'Sale ', 500000, '0', 3, '2019-12-24 04:08:40', 41, 'Published'),
(15, 7, 54, 'Nice Flat Example', 3, 3, 1, 1, '1', '1', 'This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. ', 'property.jpg', NULL, 'https://www.youtube.com/embed/a9_N1RTQYg4', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14478.476066394593!2d91.808344!3d24.8768585!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3c060575911cd31d!2sLeading%20University!5e0!3m2!1sen!2sbd!4v1576483060221!5m2!1sen!2sbd\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>', 'This is property address', '01737963893', 'kimitanzina@gmail.com', 'Rent', 500000, 'Monthly', 3, '2019-12-24 04:08:40', 41, 'Published'),
(16, 7, 54, 'Nice Flat Example', 3, 3, 1, 1, '1', '1', 'This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. ', 'property.jpg', NULL, 'https://www.youtube.com/embed/a9_N1RTQYg4', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14478.476066394593!2d91.808344!3d24.8768585!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3c060575911cd31d!2sLeading%20University!5e0!3m2!1sen!2sbd!4v1576483060221!5m2!1sen!2sbd\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>', 'This is property address', '01737963893', 'kimitanzina@gmail.com', 'Sale ', 500000, '0', 3, '2019-12-24 04:08:40', 41, 'Published'),
(17, 7, 54, 'Nice Flat Example', 3, 3, 1, 1, 'Yes', 'Yes', 'This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. ', 'property.jpg', '[\"1578929099property_galleryashraftech-travels-theme-wordpress-thumbnail.jpg\",\"1578929103property_galleryashraftech-travels-website-wordpress.jpg\",\"1579105717property_galleryashraftech-news-banglamail-featured-thumb.jpg\",\"1579192218property_galleryashrafech-mockup.jpg\"]', 'https://www.youtube.com/embed/a9_N1RTQYg4', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14478.476066394593!2d91.808344!3d24.8768585!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3c060575911cd31d!2sLeading%20University!5e0!3m2!1sen!2sbd!4v1576483060221!5m2!1sen!2sbd\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>', 'This is property address', '01737963893', 'kimitanzina@gmail.com', 'Rent', 500000, 'Monthly', 3, '2019-12-24 04:08:40', 41, 'Published'),
(19, 7, 54, 'This is a test property change', 2, 2, 1, 1, 'Yes', 'Yes', 'This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. This is room description example. ', '1578553670property_imageashrafech-mockup.jpg', 'null', 'https://www.youtube.com/embed/a9_N1RTQYg4', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14478.476066394593!2d91.808344!3d24.8768585!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3c060575911cd31d!2sLeading%20University!5e0!3m2!1sen!2sbd!4v1576483060221!5m2!1sen!2sbd\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>', 'Kamalbazar, Sylhet, Bangladesh', '01737963893', 'test@gmail.com', 'Rent With Furniture', 11500, 'Yearly', 5, '2020-01-09 07:07:50', 41, 'Published');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `data_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `data_id`, `data`) VALUES
(3, 'site_title', 'Property Mangement'),
(27, 'website_title', 'Property Management'),
(28, 'smtp_host', 'mail.sbcbsl.com'),
(29, 'smtp_port', '465'),
(30, 'smtp_user', 'salse@sbcbsl.com'),
(31, 'smtp_pass', 'AP;M,m3)gp${'),
(32, 'membership_fee', '200');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(100) NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `full_name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'passenger',
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'demo-avater.png',
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `post_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nid_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'inactive',
  `about` text COLLATE utf8_unicode_ci,
  `joined` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `email_verification` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bkash_phone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bkash_trx` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `full_name`, `user_name`, `password`, `type`, `image`, `address`, `post_code`, `phone`, `email`, `country`, `city`, `state`, `gender`, `birthday`, `nid_number`, `status`, `about`, `joined`, `email_verification`, `bkash_phone`, `bkash_trx`) VALUES
(29, 'Super', 'Admin', NULL, 'admin', '202cb962ac59075b964b07152d234b70', 'admin', '1578685380Photo00880E001E001.jpg', 'address', NULL, '01737963893', 'simukul413@gmail.com', 'bangladesh', 'Sylhet', 'sylhet', 'Male', '16/10/2018', '234', 'invective', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse error eum unde praesentium rerum aliquam adipisci aperiam nemo sunt minus, sit provident veniam ullam modi, nulla cupiditate quia aspernatur, veritatis reiciendis reprehenderit! Quia, similique eveniet nesciunt, commodi asperiores esse, voluptatem impedit id, perspiciatis assumenda error ad nostrum quam cupiditate in magnam. Officiis recusandae voluptate id illo inventore fugiat assumenda accusamus. Consequuntur saepe, asperiores deserunt. Quasi eveniet blanditiis deleniti minima voluptatem nostrum. Voluptas recusandae dolore placeat optio, quia explicabo tempore eos corporis iusto repellendus, excepturi dolorem sit quas dolorum reprehenderit unde aspernatur libero quidem ullam commodi blanditiis expedita consectetur maxime. Aliquam.', '2017-10-24 11:41:03', 'Yes', NULL, NULL),
(30, '', '', 'Owner Name four', 'owner4', '202cb962ac59075b964b07152d234b70', 'owner', 'demo-avater.png', 'Demo Address', NULL, '01737963895', 'programmerashraful@gmail.com', NULL, NULL, NULL, 'Male', NULL, NULL, 'active', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse error eum unde praesentium rerum aliquam adipisci aperiam nemo sunt minus, sit provident veniam ullam modi, nulla cupiditate quia aspernatur, veritatis reiciendis reprehenderit! Quia, similique eveniet nesciunt, commodi asperiores esse, voluptatem impedit id, perspiciatis assumenda error ad nostrum quam cupiditate in magnam. Officiis recusandae voluptate id illo inventore fugiat assumenda accusamus. Consequuntur saepe, asperiores deserunt. Quasi eveniet blanditiis deleniti minima voluptatem nostrum. Voluptas recusandae dolore placeat optio, quia explicabo tempore eos corporis iusto repellendus, excepturi dolorem sit quas dolorum reprehenderit unde aspernatur libero quidem ullam commodi blanditiis expedita consectetur maxime. Aliquam.', '2019-10-19 10:24:55', 'Yes', NULL, NULL),
(41, '', '', 'Oner name One', 'owner1', '202cb962ac59075b964b07152d234b70', 'owner', '1578511920profile_picturePhoto00089E001.jpg', 'Demo Address', '3100', '01737563895', 'owner1@gmail.com', 'Bangladesh', 'Sylhet', 'Bondor', 'Male', NULL, '657465465465', 'active', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse error eum unde praesentium rerum aliquam adipisci aperiam nemo sunt minus, sit provident veniam ullam modi, nulla cupiditate quia aspernatur, veritatis reiciendis reprehenderit! Quia, similique eveniet nesciunt, commodi asperiores esse, voluptatem impedit id, perspiciatis assumenda error ad nostrum quam cupiditate in magnam. Officiis recusandae voluptate id illo inventore fugiat assumenda accusamus. Consequuntur saepe, asperiores deserunt. Quasi eveniet blanditiis deleniti minima voluptatem nostrum. Voluptas recusandae dolore placeat optio, quia explicabo tempore eos corporis iusto repellendus, excepturi dolorem sit quas dolorum reprehenderit unde aspernatur libero quidem ullam commodi blanditiis expedita consectetur maxime. Aliquam.', '2019-10-19 10:24:55', 'Yes', '01737963893', '5GS5478EDFHA545DS5A'),
(42, '', '', 'Owner name two', 'owner2', '202cb962ac59075b964b07152d234b70', 'owner', 'demo-avater.png', 'Demo Address', NULL, '01737963895', 'owner2@gmail.com', NULL, NULL, NULL, 'Male', NULL, NULL, 'inactive', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse error eum unde praesentium rerum aliquam adipisci aperiam nemo sunt minus, sit provident veniam ullam modi, nulla cupiditate quia aspernatur, veritatis reiciendis reprehenderit! Quia, similique eveniet nesciunt, commodi asperiores esse, voluptatem impedit id, perspiciatis assumenda error ad nostrum quam cupiditate in magnam. Officiis recusandae voluptate id illo inventore fugiat assumenda accusamus. Consequuntur saepe, asperiores deserunt. Quasi eveniet blanditiis deleniti minima voluptatem nostrum. Voluptas recusandae dolore placeat optio, quia explicabo tempore eos corporis iusto repellendus, excepturi dolorem sit quas dolorum reprehenderit unde aspernatur libero quidem ullam commodi blanditiis expedita consectetur maxime. Aliquam.', '2019-10-19 10:24:55', 'Yes', NULL, NULL),
(43, '', '', 'Owner name three', 'owner3', '202cb962ac59075b964b07152d234b70', 'owner', 'demo-avater.png', 'Demo Address', NULL, '01737963895', 'owner3@gmail.com', NULL, NULL, NULL, 'Male', NULL, NULL, 'active', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse error eum unde praesentium rerum aliquam adipisci aperiam nemo sunt minus, sit provident veniam ullam modi, nulla cupiditate quia aspernatur, veritatis reiciendis reprehenderit! Quia, similique eveniet nesciunt, commodi asperiores esse, voluptatem impedit id, perspiciatis assumenda error ad nostrum quam cupiditate in magnam. Officiis recusandae voluptate id illo inventore fugiat assumenda accusamus. Consequuntur saepe, asperiores deserunt. Quasi eveniet blanditiis deleniti minima voluptatem nostrum. Voluptas recusandae dolore placeat optio, quia explicabo tempore eos corporis iusto repellendus, excepturi dolorem sit quas dolorum reprehenderit unde aspernatur libero quidem ullam commodi blanditiis expedita consectetur maxime. Aliquam.', '2019-10-19 10:24:55', 'Yes', NULL, NULL),
(44, 'Sahida', 'Sultana', 'Sahida Sultana 1', 'sahida1', '202cb962ac59075b964b07152d234b70', 'member', '1578511627profile_pictureashrafech-mockup.jpg', 'This is my new address', '3100', '01737963899', 'syl.srt@gmail.com', 'Bangladesh', 'Sylhet', 'Zindabazar', 'Female', '', '66569455456', 'inactive', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse error eum unde praesentium rerum aliquam adipisci aperiam nemo sunt minus, sit provident veniam ullam modi, nulla cupiditate quia aspernatur, veritatis reiciendis reprehenderit! Quia, similique eveniet nesciunt, commodi asperiores esse, voluptatem impedit id, perspiciatis assumenda error ad nostrum quam cupiditate in magnam. Officiis recusandae voluptate id illo inventore fugiat assumenda accusamus. Consequuntur saepe, asperiores deserunt. Quasi eveniet blanditiis deleniti minima voluptatem nostrum. Voluptas recusandae dolore placeat optio, quia explicabo tempore eos corporis iusto repellendus, excepturi dolorem sit quas dolorum reprehenderit unde aspernatur libero quidem ullam commodi blanditiis expedita consectetur maxime. Aliquam.', '2020-01-08 15:04:31', 'Yes', NULL, NULL),
(45, 'Sahida', 'Sultana', 'Sahida Sultana 2', 'sahida2', '202cb962ac59075b964b07152d234b70', 'member', '1578511627profile_pictureashrafech-mockup.jpg', 'This is my new address', '3100', '01737963799', 'syl.srt2@gmail.com', 'Bangladesh', 'Sylhet', 'Zindabazar', 'Female', '', '66569455456', 'inactive', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse error eum unde praesentium rerum aliquam adipisci aperiam nemo sunt minus, sit provident veniam ullam modi, nulla cupiditate quia aspernatur, veritatis reiciendis reprehenderit! Quia, similique eveniet nesciunt, commodi asperiores esse, voluptatem impedit id, perspiciatis assumenda error ad nostrum quam cupiditate in magnam. Officiis recusandae voluptate id illo inventore fugiat assumenda accusamus. Consequuntur saepe, asperiores deserunt. Quasi eveniet blanditiis deleniti minima voluptatem nostrum. Voluptas recusandae dolore placeat optio, quia explicabo tempore eos corporis iusto repellendus, excepturi dolorem sit quas dolorum reprehenderit unde aspernatur libero quidem ullam commodi blanditiis expedita consectetur maxime. Aliquam.', '2020-01-08 15:04:31', 'Yes', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `division_id` (`division_id`);

--
-- Indexes for table `divisions`
--
ALTER TABLE `divisions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messenger`
--
ALTER TABLE `messenger`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `districts`
--
ALTER TABLE `districts`
  MODIFY `id` int(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `divisions`
--
ALTER TABLE `divisions`
  MODIFY `id` int(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `messenger`
--
ALTER TABLE `messenger`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `properties`
--
ALTER TABLE `properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `districts`
--
ALTER TABLE `districts`
  ADD CONSTRAINT `districts_ibfk_1` FOREIGN KEY (`division_id`) REFERENCES `divisions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
