<?php
/*
    #This model for medicals data

*/
class Property_model extends CI_Model{
    public function __construct(){
        parent:: __construct();
        $this->user_id=$this->session->userdata('current_user_id');
        $this->user_type=$this->session->userdata('current_user_type');
    }
   
    
    //this function for build query for all function 
    public function search(){
        if($division_id=$this->input->get('division_id'))
            $this->db->where('division_id', $division_id);
        
        if($district_id=$this->input->get('district_id'))
            $this->db->where('district_id', $district_id);
        
        if($type=$this->input->get('type'))
            $this->db->where('type', $type);
    }
    
    
    //this method for return all medicals
    public function properties($per_page=null, $page=null, $status=null){
        $result=null;
        $this->db->order_by("id", "DESC");
        $this->search();
        if($per_page!=null){
            $this->db->limit($per_page, $page);
        }
        if($status)
            $this->db->where('status', $status);
        
        $result = $this->db->get('properties');
        return $result->result();
        
    }
    
    
    //This function for get signle property by id
    public function property($p_id){
        $result = $this->db->get_where('properties', array('id'=>$p_id));
        return $result->row(0);
    }
   
    
    /*This method for all medical count */
    public function properties_count($status=null){
        $result=null;
        
        $this->search();
         if($status)
            $this->db->where('status', $status);
        
        $result = $this->db->get('properties');
        if($result){
             return $result->num_rows();
        }else{
            return null;
        }
        
    }
    
    
    //this method for owners count
    public function properties_by_owner($user_id, $per_page=null, $page=null){
        $result=null;
        $this->db->order_by("id", "DESC");
        $this->search();
        if($per_page!=null){
            $this->db->limit($per_page, $page);
        }
        $this->db->where('user_id', $user_id);
        $result = $this->db->get('properties');
        return $result->result();
        
    }
    
    /*This method for all medical count */
    public function properties_count_by_owner($user_id){
        $result=null;
        
        $this->search();
        $this->db->where('user_id', $user_id);
        $result = $this->db->get('properties');
        if($result){
             return $result->num_rows();
        }else{
            return null;
        }
        
    }
    
    //this method for ger a single medical
    public function property_by_id($properties_id){
        $result = $this->db->get_where('properties', array('id'=>$properties_id));
        if($result){
            return $result->row(0);
        }else{
            return null;
        }
    }
    
    
}
?>