<?php  defined('BASEPATH') OR exit('No direct script access allowed');
class User_model extends CI_Model{
    public function __construct(){
        parent:: __construct();
        $this->user_type = $this->session->userdata('current_user_type');
        $this->user_id = $this->session->userdata('current_user_id');
    }
    
    
    
    public function is_user_login(){
        if($this->session->userdata('current_user_id')){
            return true;
        }else{
            return false;
        }
    }
    
    //this method will test user login 
    public function try_login($user_name, $password){
        $query = $this->db->get_where('users', array('user_name'=>$user_name, 'password'=>$password));
        if($result = $query->result()){
            $session_attr=array(
                'current_user_id'=>$result[0]->id,
                'current_user_name'=>$result[0]->user_name,
                'current_user_type'=>$result[0]->type,
            );
            $this->session->set_userdata($session_attr);
            return true;
        }else{
            return false;
        }
    }
    
    
    
    
    
    /*
    ============================================================================================
                            LOGIN SECTION
    ============================================================================================
    */
    
    
    // get all users by type and limit
    public function users($type=null, $per_page=null, $page=null, $status=null){
        if($type)
            $this->db->where('type', $type);
        if($status)
            $this->db->where('status', $status);
        
        if($per_page!=null){
            $this->db->limit($per_page, $page);
        }
        $this->db->order_by('id', 'DESC');
        
        $query = $this->db->get('users');
        return $query->result();
    }
    
    // get all users by type and limit
    public function users_count($type=null, $status=null){
        if($type)
            $this->db->where('type', $type);
        if($status)
            $this->db->where('status', $status);
        
        $this->db->order_by('id', 'DESC');
        
        $query = $this->db->get('users');
        if($query){
            return $query->num_rows();
        }else{
            return 0;
        }
    }
    
    
    // get all users by type and limit
    public function contacts($per_page=null, $page=null){
        
        if($per_page!=null and $page!=null)
            $this->db->limit($per_page, $page);
        $this->db->order_by('id', 'DESC');
        
        $query = $this->db->get('contacts');
        return $query->result();
    }
    
    // get all users by type and limit
    public function contacts_count(){
        $this->db->order_by('id', 'DESC');
        
        $query = $this->db->get('contacts');
        if($query){
            return $query->num_rows();
        }else{
            return 0;
        }
    }
    
    
    //get single user by id
    public function get_user($user_id){
        $query = $this->db->get_where('users', array('id'=>$user_id));
        if($query){
            $user = $query->row(0);
            return $user;
        }else{
            return null;
        }
    }
    
     public function get_setting_data($data_id){
        $result = $this->db->get_where('setting', array('data_id'=>$data_id));
        if($result->row(0)){
            $data = $result->row(0);
            return  $data->data;
        }else{
            return '';
        }
    }
    
    
    
    
    
    //this function for get all spalist list 
    public function specialist_list(){
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('spacialist');
        return $query->result();
    }
    
    
    public function get_specialist($id){
        $query = $this->db->get_where('spacialist', array('id'=>$id));
        if($query){
            $specialist = $query->row(0);
            return $specialist;
        }else{
            return null;
        }
    }
    
    

/*    ===================================================================================
        THIS FUNCTIONALITY FOR DOCTORS
    ===================================================================================*/
    
    // this function for doctor query builder
    public function doctor_query_build(){
        if($division_id = $this->input->get('division_id'))
            $this->db->where('doctors_info.division_id', $division_id);
        
        if($district_id = $this->input->get('district_id'))
            $this->db->where('doctors_info.district_id', $district_id);
        
        if($medical_id = $this->input->get('medical_id'))
            $this->db->where('doctors_info.medical_id', $medical_id);
        
        if($spacialist_id = $this->input->get('spacialist_id'))
            $this->db->like('doctors_info.spacialist', json_encode($spacialist_id));
        
    }
    
    // this function for get all doctors
    public function doctors($per_page=null, $page=null){
        $this->db->where('type', 'doctor');
        if($per_page!=null)
            $this->db->limit($per_page, $page);
        $this->db->order_by('users.id', 'ASC');
        $this->doctor_query_build();
        $this->db->from('users');
        $this->db->join('doctors_info', 'doctors_info.user_id = users.id');
        
        $this->db->select(array('users.id', 'users.first_name', 'users.last_name', 'users.image', 'users.full_name', 'users.phone', 'users.email'));
        
        $query = $this->db->get();
        return $query->result();
    }
    
    
    // this function for count all doctors
    public function doctors_count(){
        $this->db->where('type', 'doctor');
        $this->doctor_query_build();
        $this->db->from('users');
        $this->db->join('doctors_info', 'doctors_info.user_id = users.id');
        
        $this->db->select(array('users.id', 'users.first_name', 'users.last_name', 'users.image', 'users.full_name', 'users.phone', 'users.email'));
        
        $query = $this->db->get();
        
        if($query){
            return $query->num_rows();
        }else{
            return 0;
        }
    }
    
    //this functions for doctor 
    public function doctor_info($user_id){
        $result = $this->db->get_where('doctors_info', array('user_id'=>$user_id));
        $data = $result->row(0);
        return  $data;
    }
    
    
    //this function for build query for doctor apointments
    
    
    
    // this function for get appoint ment count by date
    public function appointments($doctor_id=null, $from_date=null, $to_date=null, $patient_id=null){
        $this->db->order_by('id', 'ASC');
        if($doctor_id)
            $this->db->where('doctor_id', $doctor_id);
        
        if($patient_id)
            $this->db->where('patient_id', $patient_id);
        
        if($to_date and $to_date)
            $this->db->where("appoint_date BETWEEN '$from_date' AND '$to_date'");
        
        $query = $this->db->get('appointments');
        return $query->result();
        
    }
    
    //get a single appointment data by id
    public function appointment($appointment_id){
        $result = $this->db->get_where('appointments', array('id'=>$appointment_id));
        $data = $result->row(0);
        return  $data;
    }
    
    // this function for get appoint ment count by date
    public function appointments_count($doctor_id=null, $from_date=null, $to_date=null, $patient_id=null){
        if($doctor_id)
            $this->db->where('doctor_id', $doctor_id);
        
        if($patient_id)
            $this->db->where('patient_id', $patient_id);
        
        if($to_date and $to_date)
            $this->db->where("appoint_date BETWEEN '$from_date' AND '$to_date'");
        
        $query = $this->db->get('appointments');
        return $query->num_rows();
        
    }
    
    
    //this function for get new message
    public function my_message_count(){
        $this->db->where('receive_id', $this->user_id);
        $this->db->where('status', 'new');
        $query = $this->db->get('messenger');
        return $query->num_rows();
    }
    
    
    
    // this funciton for get reviews
    public function get_reviews($doctor_id){
        $this->db->where('receive_id', $doctor_id);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('review');
        return $query->result();
    }
    
    // avarage ratings of a doctor
    public function av_rating($docrot_id){
        $this->db->where('receive_id', $docrot_id);
        $this->db->select_avg('rating');
        $query = $this->db->get('review');
        return $query->row(0)->rating;
    }
    
    // get rating text
    public function get_ratings($docrot_id=null, $rating=null){
        if(!$rating)
            $rating = ceil($this->av_rating($docrot_id));
        $total = 5;
        $i=1;
        $text='No review yet';
        $starts=null;
        if($rating){
            for($i=1; $i<=$total; $i++){
                if($i<=$rating){
                    $starts.='<i class="fa fa-star"></i>';
                }else{
                    $starts.='<i class="fa fa-star-o"></i>';
                }
            }
            return $starts;
        }else{
            return $text;
        }
    }
    
    
    /*This method for all medical count */
    public function properties_count($status=null){
        $result=null;
        
         if($status)
            $this->db->where('status', $status);
        
        $result = $this->db->get('properties');
        if($result){
             return $result->num_rows();
        }else{
            return null;
        }
        
    }
    
    
    
    // this method for from date
    public function mk_from_date($date){
        $search_date=null;
        if($date){
            $date_parts = explode('/', $date);
            $search_date = $date_parts[2].'-'.$date_parts[1].'-'.$date_parts[0].' 0:0:0';
        }
        return $search_date;
    }
    
    // this method for to date
    public function mk_to_date($date){
        $search_date=null;
        if($date){
            $date_parts = explode('/', $date);
            $search_date = $date_parts[2].'-'.$date_parts[1].'-'.$date_parts[0].' 23:59:59';
        }
        return $search_date;
    }
    
    
    
}