<?php  defined('BASEPATH') OR exit('No direct script access allowed');
class Messenger_model extends CI_Model{
    public function __construct(){
        parent:: __construct();
        $this->user_type = $this->session->userdata('current_user_type');
        $this->user_id = $this->session->userdata('current_user_id');
    }
    
  
    public function messages($receiver_id){
        
        $this->db->where('sender_id='.$this->user_id.' AND receive_id='.$receiver_id);
        $this->db->OR_where('receive_id='.$this->user_id.' AND sender_id='.$receiver_id);
        
        
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get('messenger');
        return $query->result();
    }
    
    //this function for find users in messages
    public function message_users(){
        $this->db->where('messenger.sender_id',  $this->user_id );
        $this->db->or_where('messenger.receive_id',  $this->user_id );
        $this->db->order_by('messenger.id', 'DESC');
        $this->db->select('users.id, users.image, users.full_name, messenger.message, messenger.date');
        $this->db->from('users');
        $this->db->group_by('users.id');
        $this->db->join('messenger', 'messenger.receive_id = users.id OR messenger.sender_id = users.id AND users.id!='.$this->user_id, 'right');
        $query = $this->db->get();
        return $query->result();
    }
    
    
 /*   public function message_users(){
        $this->db->where('messenger.sender_id',  $this->user_id );
        $this->db->or_where('messenger.receive_id',  $this->user_id );
        $this->db->order_by('messenger.id', 'DESC');
        $this->db->select('users.id, users.image, users.full_name, messenger.message, messenger.date');
        $this->db->from('users');
        $this->db->group_by('users.id');
        $this->db->join('messenger', 'users.id = messenger.receive_id OR users.id = messenger.sender_id');
        $query = $this->db->get();
    }
    */
  
    
    
}