<?php  defined('BASEPATH') OR exit('No direct script access allowed');
class Ajax_model extends CI_Model{
    public function __construct(){
        parent:: __construct();
    }
    
    // Get all conacts from contact table
    public function divisions(){
        $this->db->order_by('name', 'ASC');
        $query = $this->db->get('divisions');
        return $query->result();
    }
    
    
    public function districts($divisions_id=null){
        if($divisions_id)
            $this->db->where('division_id', $divisions_id);
        $this->db->order_by('name', 'ASC');
        $query = $this->db->get('districts');
        return $query->result();
    }
    
    public function medicals($district_id=null){
        if($district_id)
            $this->db->where('district_id', $district_id);
        $this->db->order_by('medical_name', 'ASC');
        $query = $this->db->get('medicals');
        return $query->result();
    }
    
    //get single Division
    public function get_division($division_id){
        $query = $this->db->get_where('divisions', array('id'=>$division_id));
        return $query->row(0);
    }
    
    //get single district
    public function get_district($district_id){
        $query = $this->db->get_where('districts', array('id'=>$district_id));
        return $query->row(0);
    }
    
    // get division by district id
    public function get_division_by_district_id($district_id){
        $query = $this->db->get_where('districts', array('id'=>$district_id));
        $district = $query->row(0);
        if($district){
            return $this->get_division($district->division_id);
        }else{
            return null;
        }
    }
    
    
}