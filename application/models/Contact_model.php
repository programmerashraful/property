<?php  defined('BASEPATH') OR exit('No direct script access allowed');
class Contact_model extends CI_Model{
    public function __construct(){
        parent:: __construct();
    }
    
    // Get all conacts from contact table
    public function contacts($per_page=null, $page=null){
        if($per_page!=null and $page!=null)
            $this->db->limit($per_page, $page);
        $this->db->order_by('id', 'DESC');
        
        $query = $this->db->get('contacts');
        return $query->result();
    }
    
    // get all contucts row count
    public function contacts_count(){
       
        $this->db->order_by('id', 'DESC');
        
        $query = $this->db->get('contacts');
        if($query){
            return $query->num_rows();
        }else{
            return 0;
        }
    }
    
    //get contact by contact table ID
    public function get_contact($contact_id){
        $query = $this->db->get_where('contacts', array('id'=>$contact_id));
        if($query){
            $user = $query->row(0);
            return $user;
        }else{
            return null;
        }
    }
    
    
}