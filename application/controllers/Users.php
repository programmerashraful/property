<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends User_controller {
    public function __construct(){
        parent::__construct();
        $this->user_type = $this->session->userdata('current_user_type');
        $this->user_id = $this->session->userdata('current_user_id');
        $this->load->model('property_model');
        $this->load->model('messenger_model');
        $this->check_email_verification();
        $this->check_membership();
        
    }
    
	public function index()
	{
        $this->dashboard();
    }
    
    //this function for load dashboard 
    public function dashboard(){
        $this->load->view('users/dashboard');
    }
    
    
    //this function for profile details
    public function profile(){
        $user = $this->user_model->get_user($this->user_id);
        $data['user'] = $user;
        $this->load->view('users/profile', $data);
    }
    
    //this function for edit profile  
    public function edit_profile(){
        $this->save_profile();
        $user = $this->user_model->get_user($this->user_id);
        $data['user'] = $user;
        $this->load->view('users/edit-profile', $data);
    }
    
    
    //this method for save profile 
    public function save_profile(){
        if($this->input->post('save_profile')){
            
            $this->form_validation->set_rules('full_name', 'Full Name', 'required|trim');
            $this->form_validation->set_rules('address', 'Address ', 'required|trim');
            $this->form_validation->set_rules('password', 'Password', 'min_length[6]');
            $this->form_validation->set_rules('phone', 'Phone', 'required|callback_unique_phone['.$this->user_id.']|min_length[11]|max_length[11]');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_unique_email['.$this->user_id.']');
            
            if($this->form_validation->run()){
                $user_attr = array(
                    'full_name'=>$this->input->post('full_name'),
                    'phone'=>$this->input->post('phone'),
                    'email'=>$this->input->post('email'),
                    'address'=>$this->input->post('address'),
                    'country'=>$this->input->post('country'),
                    'city'=>$this->input->post('city'),
                    'state'=>$this->input->post('state'),
                    'gender'=>$this->input->post('gender'),
                    'nid_number'=>$this->input->post('nid_number'),
                    'post_code'=>$this->input->post('post_code'),
                    'about'=>addslashes($this->input->post('about')),
                );
                
                if($password = $this->input->post('password'))
                    $user_attr['password']=md5($password);
                
                //if profile picture upload
                if($_FILES['profile_picture']["tmp_name"]){
                    if($file = $this->do_upload('./uploads/users/', 'profile_picture')){
                      $user_attr['image'] = $file; 
                        $this->image_resize('./uploads/users/'.$file);
                    }
                }
                
                
                //if save user
                if($this->db->where('id', $this->user_id)->update('users', $user_attr)){
                    
                    $this->session->set_userdata('success_msg', 'Your data update sucessfully');
                }else{
                     $this->session->set_userdata('error_msg', 'Something wrong please try agian.');
                }
                
                
            }else{
                $this->session->set_userdata('error_msg', validation_errors('<p>', '</p>'));
            }
        }
        
    }
    
    
    
    
    
    //this this function for list of all property of this owner
    public function property($current_page=null){
        $owner_id=$this->user_id;
        $this->owner_auth();
        
        $this->load->library('pagination');
        $config['base_url'] = base_url().'users/property/';
        $config['use_page_numbers'] = true;
        $config['reuse_query_string'] = true;
        $config['total_rows'] = $this->property_model->properties_count_by_owner($owner_id);
		$config['per_page'] = 9;
        $config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['last_link'] = 'LAST →';
		$config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['first_tag_close'] = '</span></li>';
		$config['first_link'] = '← FIRST';
		$config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['last_tag_close'] = '</span></li>';
		$config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close'] = '</span></li>';
		$config['cur_tag_open'] = '<a class="page-link">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = '';
		$config['prev_link'] = '';
		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['prev_tag_close'] = '</a></li>';
		$config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['next_tag_close'] = '</span></li>';
		$config['prev_link'] = ' ← ';
		$config['next_link'] = ' → ';
        
        
        $this->pagination->initialize($config); 
        $data['links']=$this->pagination->create_links();
        
      
        
		$data['current_page']=$current_page;
        $data['properties_counter'] = $config['total_rows'];
        if($current_page)
            $start_data = $current_page*$config['per_page']-$config['per_page'];
        else
            $start_data=0;
        //echo $start_data; exit();
        $data['properties'] = $this->property_model->properties_by_owner($owner_id, $config['per_page'], $start_data);
       
        $this->load->view('users/property', $data);
    }
    
    
    //this function for add a property
    public function add_new_property(){
        $this->owner_auth();
        if($this->input->post('save_property')){
            $attr=array(
                'user_id'           => $this->user_id,
                'title'             => $this->input->post('title'),
                'division_id'       => $this->input->post('division_id'),
                'district_id'       => $this->input->post('district_id'),
                'bed_room'          => $this->input->post('bed_room'),
                'bath_room'         => $this->input->post('bath_room'),
                'dining_room'       => $this->input->post('dining_room'),
                'drawing_room'      => $this->input->post('drawing_room'),
                'lift'              => $this->input->post('lift'),
                'garage'            => $this->input->post('garage'),
                'description'       => $this->input->post('description'),
                'youtube_video'     => $this->input->post('youtube_video'),
                'google_location'   => $this->input->post('google_location'),
                'google_location'   => $this->input->post('google_location'),
                'address'           => $this->input->post('address'),
                'phone'             => $this->input->post('phone'),
                'email'             => $this->input->post('email'),
                'type'              => $this->input->post('type'),
                'price'             => $this->input->post('price'),
                'rent_type'         => $this->input->post('rent_type'),
                'rating'            => $this->input->post('rating'),
                'property_images'   => json_encode($this->input->post('property_images')),
            );
            //if profile property image
            if($_FILES['property_image']["tmp_name"]){
                if($file = $this->do_upload('./uploads/property/', 'property_image')){
                  $attr['image'] = $file; 
                    $this->image_resize('./uploads/property/'.$file, 550, 310);
                }
            }
            
            if($this->db->insert('properties', $attr)){
                $this->session->set_userdata('success_msg', 'You have successfully add a property. Your property will publish after admin approval.');
                redirect('users/property');
            }else{
                $this->session->set_userdata('error_msg', 'Something wrong please try again.');
            }
        }
        
        $this->load->view('users/add-property');
    }
    
    
    //this function for edit property
    
    public function property_edit($property_id=null){
        $this->owner_auth();
        if(!$property_id or !is_numeric($property_id))
            redirect('users/property');
        $property = $this->property_model->property($property_id);
        if(!$property or $property->user_id!=$this->user_id)
            redirect('users/property');
        
       if($this->input->post('save_property')){
            $attr=array(
                'title'             => $this->input->post('title'),
                'division_id'       => $this->input->post('division_id'),
                'district_id'       => $this->input->post('district_id'),
                'bed_room'          => $this->input->post('bed_room'),
                'bath_room'         => $this->input->post('bath_room'),
                'dining_room'       => $this->input->post('dining_room'),
                'drawing_room'      => $this->input->post('drawing_room'),
                'lift'              => $this->input->post('lift'),
                'garage'            => $this->input->post('garage'),
                'description'       => $this->input->post('description'),
                'youtube_video'     => $this->input->post('youtube_video'),
                'google_location'   => $this->input->post('google_location'),
                'google_location'   => $this->input->post('google_location'),
                'address'           => $this->input->post('address'),
                'phone'             => $this->input->post('phone'),
                'email'             => $this->input->post('email'),
                'type'              => $this->input->post('type'),
                'price'             => $this->input->post('price'),
                'rent_type'         => $this->input->post('rent_type'),
                'rating'            => $this->input->post('rating'),
                'property_images'   => json_encode($this->input->post('property_images')),
            );
            //if profile property image
            if($_FILES['property_image']["tmp_name"]){
                if($file = $this->do_upload('./uploads/property/', 'property_image')){
                  $attr['image'] = $file; 
                    $this->image_resize('./uploads/property/'.$file, 550, 310);
                }
            }


            if($this->db->where('id', $property_id)->update('properties', $attr)){
                $this->session->set_userdata('success_msg', 'You have successfully update property.');
            }else{
                $this->session->set_userdata('error_msg', 'Something wrong please try again.');
            }
       }
        $property = $this->property_model->property($property_id);
        $data['property'] = $property;
        $this->load->view('users/edit-property', $data);
    }
    
    
    //this function for delete property
    public function property_delete($property_id=null){
        $this->owner_auth();
        if(!$property_id or !is_numeric($property_id))
            redirect('users/property');
        $property = $this->property_model->property($property_id);
        if(!$property or $property->user_id!=$this->user_id)
            redirect('users/property');
        if($this->db->where('id', $property_id)->delete('properties')){
            $this->session->set_userdata('success_msg', 'You have successfully delete a property.');
        }else{
            $this->session->set_userdata('error_msg', 'Something wrong please try again.');
        }
        redirect('users/property');
    }
    
    //this function for email  verification
    public function email_verification($verification_key=null){
        $user = $this->user_model->get_user($this->user_id);
        
        
        if($user->email_verification){
            $this->session->set_userdata('success_msg', 'You have already verified your email.');
            redirect('users');
        }
        
        if($this->input->post('submit_verification_email')){
            $change_log=array();
                $this->load->helper('string');
                $key = random_string('alnum', 30);
                $change_log['email']=$user->email;
                $change_log['email_verification_key']=$key;
                $this->session->set_userdata($change_log);
                
                
                //send email
                if($this->send_email($change_log['email'], $key)){
                    $this->session->set_userdata('success_msg', 'Thank you your email verificaiton link has been sent to your email ('.$change_log['email'].'). Please check your email inbox or spam folder');
                }else{
                    $this->session->set_userdata('error_msg', 'Email sending problem please try again');
                }
        }
        
        //if send email verification key
        if($verification_key and $verification_key==$this->session->userdata('email_verification_key')){
            $this->db->where('id', $this->user_id)->update('users', array('email_verification'=>'Yes'));
            redirect('users');
        }else{
            //$this->session->set_userdata('error_msg', 'Wrong Email verification key please get a new email.');
        }
        
        $this->load->view('users/email_verification');
    }
    
    //this function for check email verification
    private function check_email_verification(){
        $user = $this->user_model->get_user($this->user_id);
        $status=null;
        $template = $this->uri->segment(2);
        
        if($user)
            $status = $user->email_verification;
        
        if(!$status and $template!='email_verification' and $template!='membership'){
            redirect('users/email_verification');
        }
        
    }
    
    
    //this function for check membership
    private function check_membership(){
        $user = $this->user_model->get_user($this->user_id);
        $status=null;
        $template = $this->uri->segment(2);
        
        if($user)
            $status = $user->status;
        
        if($status!='active' and $template!='membership' and $template!='email_verification' and $this->user_type=='owner'){
            redirect('users/membership');
        }
    }
    
    
    //this function for set membershp transations
    public function membership(){
        if($this->input->post('save_payment_info')){
            $attr = array(
                'bkash_phone'=>$this->input->post('bkash_phone'),
                'bkash_trx'=>$this->input->post('bkash_trx')
            );
            if($this->db->where('id', $this->user_id)->update('users', $attr)){
                 $this->session->set_userdata('success_msg', 'Your payment information hasbeen sent to our admin desk. We will review the information and confirm your membership. It may take 24 to 72  business hours. Thank you.');
            }else{
                 $this->session->set_userdata('error_msg', 'Somethin whorng please try again.');
            }
        }
        $data['user'] = $this->user_model->get_user($this->user_id);
        $this->load->view('users/membership', $data);
    }
    
    //this function for owner check 
    private function owner_auth(){
        if($this->user_type!='owner')
            redirect('users');
    }
    
    //this funciton for check unique email
    public function unique_email($email, $user_id){
        
        $this->db->where_not_in('id', $user_id);
        $this->db->where_in('email', $email);
        $results = $this->db->get('users');
        if($results->result()){
            $this->form_validation->set_message('unique_email', 'Your email already used. Please try another email');
            return false;
        }else{
            return true;
        }
    }
    
    
    //this funciton for check unique email
    public function unique_phone($phone, $user_id){
        $this->db->where_not_in('id', $user_id);
        $this->db->where_in('phone', $phone);
        $results = $this->db->get('users');
        if($results->result()){
            $this->form_validation->set_message('unique_phone', 'Your phone already used. Please try another phone');
            return false;
        }else{
            return true;
        }
    }
    
    
    
    /*This is a fucntion that upload the hotel image*/
    private function do_upload($upload_path, $file_input_field){
        $path = $upload_path; 
        $fileTmpName=$_FILES[$file_input_field]["tmp_name"];
        $upload_dir = $upload_path; 
        $file_name = $_FILES[$file_input_field]['name'];
        $type = explode('.', $file_name);
        $type = $type[count($type)-1];
        $time = time();
        if( in_array($type, array('jpg', 'png', 'jpeg', 'gif', 'JPEG', 'PNG', 'JPG', 'GIF' )) ){
            if( is_uploaded_file( $_FILES[$file_input_field]['tmp_name'] ) ){
                move_uploaded_file( $_FILES[$file_input_field]['tmp_name'], $upload_dir.$time.$file_input_field.$file_name );
                return $time.$file_input_field.$file_name;
            }
        }else{
            $this->session->set_userdata('error_msg', 'File type not supported');
            return false;
        }
   }
    
    
    //this function for resize image
    private function image_resize($source, $width=250, $height=250){
        $this->load->library('image_lib');
        $config['image_library'] = 'gd2';
        $config['source_image'] = $source;
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = FALSE;
        $config['width']     = $width;
        $config['height']   = $height;

        $this->image_lib->clear();
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }
    
    
    
    
    //this function for send email 
    public function send_email($email=null, $key=null){
        $link = site_url('users/email_verification/'.$key);
        $style = 'style="display:inline-block;background:green;border-radius:5px;color:yellow;padding:5px 10px"';
        $message = 'Email verification link:- Please click the link bellow. <br /> <a href="'.$link.'" '.$style.'> Confirm Email </a>';
        
        $this->load->library('email');

        $this->email->set_newline("\r\n");

        $config['protocol'] = 'smtp';
        $config['smtp_host'] = $this->user_model->get_setting_data('smtp_host');
        $config['smtp_port'] = $this->user_model->get_setting_data('smtp_port');
        $config['smtp_user'] = $this->user_model->get_setting_data('smtp_user');
        $config['smtp_from_name'] = $this->user_model->get_setting_data('site_title');
        $config['smtp_pass'] = $this->user_model->get_setting_data('smtp_pass');
        $config['wordwrap'] = TRUE;
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html';                       
        $config['smtp_crypto'] = 'ssl';                       

        $this->email->initialize($config);

        $this->email->from($config['smtp_user'], $config['smtp_from_name']);
        $this->email->to($email);
        $this->email->subject('EMAIL VERIFICATION');

        $this->email->message($message);
        return $this->email->send();
        
        
    }
    
    //this function for image upload error
    public function ajax_image_upload(){
        header('Content-Type: application/json');
        $data=array();
        if($file = $this->do_upload('./uploads/property/', 'property_gallery')){
            $data['error']=false;
            $data['file']=$file;
        }else{
            $data['error']=true;
            $data['file']=null;
        }
        echo json_encode($data);
    }
    
    
    
    /*
    ===========================================================
                    MESSENGER START HERE
    ===========================================================
    */
    
    public function start_threat(){
        
    }
    
    public function messenger($with=null){
        $data['messenger_users'] = $this->messenger_model->message_users();
        if(!$with){
            if($messenger_users = $data['messenger_users']){
                foreach($messenger_users as $redirect_user){
                    if($redirect_user->id!=$this->user_id){
                        redirect('users/messenger/'.$redirect_user->id);
                        break;
                    }
                }}
        }
        
        $user = $this->user_model->get_user($with);
        /*if(!$user or $this->user_id==$user->id)
            redirect('users');*/
        
        //if take action in send message
        $this->send_message($with);
        $data['messenger_users'] = $this->messenger_model->message_users();
        $data['user'] = $user;
        $data['with_id'] = $with;
        $data['messages'] = $this->messenger_model->messages($with);
        if($user)
            $this->set_read($with);
        $this->load->view('users/messenger', $data);
    }
    
    
    
    //this function for send message
    protected function send_message($with){
        if($this->input->post('send_message')){
            $attr=array(
                'message'=>addslashes($this->input->post('message')),
                'sender_id'=>$this->user_id,
                'receive_id'=>$with,
            );
            
            if($this->db->insert('messenger', $attr)){
                
            }else{
                
            }
        }
    }
    
    
    //read messages 
    private function set_read($with){
        $this->db->where('sender_id', $with);
        $this->db->where('receive_id', $this->user_id);
        $this->db->update('messenger', array('status'=>'read'));
    }
    
}
