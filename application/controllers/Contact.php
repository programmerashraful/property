<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {
    
	public function index()
	{
        
        
        if($this->input->post('send_message')){
            $this->form_validation->set_rules('person_name', 'Name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('subject', 'Subject', 'trim|required');
            $this->form_validation->set_rules('message', 'Message', 'trim|required');
            
            
            if($this->form_validation->run()){
                //if this form validation is correct
                $data_attr = array(
                    'name'=>$this->input->post('person_name'),
                    'email'=>$this->input->post('email'),
                    'subject'=>$this->input->post('subject'),
                    'message'=>$this->input->post('message')
                );
                
                
                if($this->db->insert('contacts', $data_attr)){
                    //If data push to database table
                    $this->session->set_userdata('success_msg', 'Your message has been send to admin. We will review your message');
                }else{
                    //If not submit data to dtabase table
                    $this->session->set_userdata('error_msg', 'Something wrong try again.');
                }
            }else{
                //if the form validation is not correct
                $errors = validation_errors('<p>', '</p>');
                $this->session->set_userdata('error_msg', $errors);
            }
            
        }
        
		$this->load->view('frontend/contact');
	}
}
