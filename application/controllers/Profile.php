<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->model('property_model');
        if(!$this->user_model->is_user_login())
            redirect();
    }
    
	public function index()
	{
		//Blank
	}
    
    
    
    //This function for view owner profile 
    public function owner($owner_id=null){
        
        if(!$owner_id or !is_numeric($owner_id))
            redirect('profile/owners');
        
        $user = $this->user_model->get_user($owner_id);
       
        if(!$user)
            redirect('profile/owners');
        $data['total_property'] = $this->property_model->properties_count_by_owner($owner_id);
        $data['owner'] = $user;
        
        $this->load->view('frontend/owner-profile', $data);
    }
    
    //this function for all owner list 
    public function owners($current_page=null){
        
        $this->load->library('pagination');
        $config['base_url'] = base_url().'profile/owners/';
        $config['use_page_numbers'] = true;
        $config['reuse_query_string'] = true;
        $config['total_rows'] = $this->user_model->users_count('owner');
		$config['per_page'] = 10;
        $config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['last_link'] = 'LAST →';
		$config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['first_tag_close'] = '</span></li>';
		$config['first_link'] = '← FIRST';
		$config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['last_tag_close'] = '</span></li>';
		$config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close'] = '</span></li>';
		$config['cur_tag_open'] = '<a class="page-link">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = '';
		$config['prev_link'] = '';
		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['prev_tag_close'] = '</a></li>';
		$config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['next_tag_close'] = '</span></li>';
		$config['prev_link'] = ' ← ';
		$config['next_link'] = ' → ';
        
        
        $this->pagination->initialize($config); 
        $data['links']=$this->pagination->create_links();
        
      
        
		$data['current_page']=$current_page;
        $data['properties_counter'] = $config['total_rows'];
        if($current_page)
            $start_data = $current_page*$config['per_page']-$config['per_page'];
        else
            $start_data=0;
        $data['owners'] = $this->user_model->users('owner', $config['per_page'], $start_data);
        $this->load->view('frontend/owner-list', $data);
    }
    
    
    //this function for get owners property 
    public function owner_property($current_page=null){
        $owner_id=$this->input->get('owner');
        if(!$owner_id or !is_numeric($owner_id))
            redirect('profile/owners');
        
        $user = $this->user_model->get_user($owner_id);
       
        if(!$user)
            redirect('profile/owners');
        
        
        $this->load->library('pagination');
        $config['base_url'] = base_url().'profile/owner_property/';
        $config['use_page_numbers'] = true;
        $config['reuse_query_string'] = true;
        $config['total_rows'] = $this->property_model->properties_count_by_owner($owner_id);
		$config['per_page'] = 12;
        $config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['last_link'] = 'LAST →';
		$config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['first_tag_close'] = '</span></li>';
		$config['first_link'] = '← FIRST';
		$config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['last_tag_close'] = '</span></li>';
		$config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close'] = '</span></li>';
		$config['cur_tag_open'] = '<a class="page-link">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = '';
		$config['prev_link'] = '';
		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['prev_tag_close'] = '</a></li>';
		$config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['next_tag_close'] = '</span></li>';
		$config['prev_link'] = ' ← ';
		$config['next_link'] = ' → ';
        
        
        $this->pagination->initialize($config); 
        $data['links']=$this->pagination->create_links();
        
      
        
		$data['current_page']=$current_page;
        $data['properties_counter'] = $config['total_rows'];
        if($current_page)
            $start_data = $current_page*$config['per_page']-$config['per_page'];
        else
            $start_data=0;
        //echo $start_data; exit();
        $data['properties'] = $this->property_model->properties_by_owner($owner_id, $config['per_page'], $start_data);
        $data['owner'] = $user;
        $this->load->view('frontend/owner-property', $data);
    }
    
}
