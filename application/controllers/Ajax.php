<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('ajax_model');
    }
    
	public function index()
	{
		
	}
    
    
    public function get_divisions(){
        header('Content-Type: application/json');
        $divisions = $this->ajax_model->divisions();
        echo json_encode($divisions);
        
    }
    
    public function get_districts($divisions_id=null){
        header('Content-Type: application/json');
        $districts = $this->ajax_model->districts($divisions_id);
        echo json_encode($districts);
        
    }
    
    
}
