<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Property extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('property_model');
        $this->load->model('ajax_model');
        $this->user_type = $this->session->userdata('current_user_type');
    }
    
    // This method for all medical
    public function index($current_page=null){
        $this->load->library('pagination');
        $config['base_url'] = base_url().'property/index/';
        $config['use_page_numbers'] = true;
        $config['reuse_query_string'] = true;
        $config['total_rows'] = $this->property_model->properties_count('Published');
		$config['per_page'] = 12;
        $config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['last_link'] = 'LAST →';
		$config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['first_tag_close'] = '</span></li>';
		$config['first_link'] = '← FIRST';
		$config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['last_tag_close'] = '</span></li>';
		$config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close'] = '</span></li>';
		$config['cur_tag_open'] = '<a class="page-link">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = '';
		$config['prev_link'] = '';
		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['prev_tag_close'] = '</a></li>';
		$config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['next_tag_close'] = '</span></li>';
		$config['prev_link'] = ' ← ';
		$config['next_link'] = ' → ';
        
        
        $this->pagination->initialize($config); 
        $data['links']=$this->pagination->create_links();
        
       
		$data['current_page']=$current_page;
        $data['properties_counter'] = $config['total_rows'];
        if($current_page)
            $start_data = $current_page*$config['per_page']-$config['per_page'];
        else
            $start_data=0;
        //echo $start_data; exit();
        $data['properties'] = $this->property_model->properties($config['per_page'], $start_data, 'Published');
        
        
        $this->load->view('frontend/property-list', $data);
        
    }
    
    //this function for view property
    public function view($p_id=null){
        if(!$p_id or !is_numeric($p_id))
            redirect('property');
        $property = $this->property_model->property($p_id);
        if(!$property)
            redirect('property');
        
        if($property->status=='Requested'){
            if($this->user_type=='owner' or $this->user_type=='admin'){}else{redirect('property');}
                
        }
        
        $data['property'] = $property;
        $data['owner'] = $this->user_model->get_user($property->user_id);
        
        $this->load->view('frontend/single-property', $data);
    }
    
    
}

