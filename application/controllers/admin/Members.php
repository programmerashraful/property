<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Members extends Admin_controller {
    public function __construct(){
        parent::__construct();
        
    }
    
	public function index()
	{
        $data['members'] = $this->user_model->users('member', null, null);
        $this->load->view('admin/members/members', $data);
    }
    
    
    
    //this function for delete user
    public function delete($member_id=null){
        if(!$member_id or !is_numeric($member_id))
            redirect('members');
        if($this->db->where('id', $member_id)->delete('users')){
            $this->session->set_userdata('succes_msg', 'Owner deletion successfull.');
        }else{
            $this->session->set_userdata('error_msg', 'Something wrong please try again');
        }
        
        redirect('members');
    }
    
    
    
    
}
