<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contacts extends Admin_controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('contact_model');
    }
    
	public function index()
	{
        $data['contacts'] = $this->contact_model->contacts();
        $this->load->view('admin/contacts/all_contact', $data);
    }
    
    //delete method
    public function delete($contact_id=null){
        if($contact_id and is_numeric($contact_id)){
            if($this->db->where('id', $contact_id)->delete('contacts')){
                $this->session->set_userdata('success_msg', 'Contact Deletion successfull');
            }else{
                $this->session->set_userdata('error_msg', 'Something wrong please try again.');
            }
        }
        redirect('admin/contacts/');
    }
}
