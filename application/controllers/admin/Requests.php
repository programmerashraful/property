<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Requests extends Admin_controller {
    public function __construct(){
        parent::__construct();
        
    }
    
	public function index()
	{
        $data['owners'] = $this->user_model->users('owner', null, null, 'inactive');
        $this->load->view('admin/owners/requests', $data);
    }
    
    //this function for active user
    public function active($owner_id=null){
        if(!$owner_id or !is_numeric($owner_id))
            redirect('admin/requests');
        if($this->db->where('id', $owner_id)->update('users', array('status'=>'active'))){
            $this->session->set_userdata('succes_msg', 'This owner has been active now.');
        }else{
            $this->session->set_userdata('error_msg', 'Something wrong please try again');
        }
        
        redirect('admin/requests');
    }
    
    //this function for delete user
    public function delete($owner_id=null){
        if(!$owner_id or !is_numeric($owner_id))
            redirect('admin/requests');
        if($this->db->where('id', $owner_id)->delete('users')){
            $this->session->set_userdata('succes_msg', 'Owner deletion successfull.');
        }else{
            $this->session->set_userdata('error_msg', 'Something wrong please try again');
        }
        
        redirect('admin/requests');
    }
    
    
    
    
}
