<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Owners extends Admin_controller {
    public function __construct(){
        parent::__construct();
        
    }
    
	public function index()
	{
        $data['owners'] = $this->user_model->users('owner', null, null, 'active');
        $this->load->view('admin/owners/owners', $data);
    }
    
    //this function for active user
    public function inactive($owner_id=null){
        if(!$owner_id or !is_numeric($owner_id))
            redirect('admin/owners');
        if($this->db->where('id', $owner_id)->update('users', array('status'=>'inactive'))){
            $this->session->set_userdata('succes_msg', 'This owner has been inactive now.');
        }else{
            $this->session->set_userdata('error_msg', 'Something wrong please try again');
        }
        
        redirect('admin/owners');
    }
    
    //this function for delete user
    public function delete($owner_id=null){
        if(!$owner_id or !is_numeric($owner_id))
            redirect('admin/owners');
        if($this->db->where('id', $owner_id)->delete('users')){
            $this->session->set_userdata('succes_msg', 'Owner deletion successfull.');
        }else{
            $this->session->set_userdata('error_msg', 'Something wrong please try again');
        }
        
        redirect('admin/owners');
    }
    
    
    
    
}
