<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Property extends Admin_controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('property_model');
        
    }
    
	public function index($current_page=null)
	{
        $this->load->library('pagination');
        $config['base_url'] = base_url().'admin/property/index/';
        $config['use_page_numbers'] = false;
        $config['reuse_query_string'] = true;
        $config['total_rows'] = $this->property_model->properties_count('Published');
		$config['per_page'] = 12;
        $config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['last_link'] = 'LAST →';
		$config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['first_tag_close'] = '</span></li>';
		$config['first_link'] = '← FIRST';
		$config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['last_tag_close'] = '</span></li>';
		$config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close'] = '</span></li>';
		$config['cur_tag_open'] = '<a class="page-link">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = '';
		$config['prev_link'] = '';
		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['prev_tag_close'] = '</a></li>';
		$config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['next_tag_close'] = '</span></li>';
		$config['prev_link'] = ' ← ';
		$config['next_link'] = ' → ';
        
        
        $this->pagination->initialize($config); 
        $data['links']=$this->pagination->create_links();
        
       
		$data['current_page']=$current_page;
       
        $data['properties'] = $this->property_model->properties($config['per_page'], $current_page, 'Published');
        $this->load->view('admin/property/published', $data);
    }
    
    
    public function requested($current_page=null){
        $this->load->library('pagination');
        $config['base_url'] = base_url().'admin/property/requested/';
        $config['use_page_numbers'] = false;
        $config['reuse_query_string'] = true;
        $config['total_rows'] = $this->property_model->properties_count('Requested');
		$config['per_page'] = 12;
        $config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['last_link'] = 'LAST →';
		$config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['first_tag_close'] = '</span></li>';
		$config['first_link'] = '← FIRST';
		$config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['last_tag_close'] = '</span></li>';
		$config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close'] = '</span></li>';
		$config['cur_tag_open'] = '<a class="page-link">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = '';
		$config['prev_link'] = '';
		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['prev_tag_close'] = '</a></li>';
		$config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['next_tag_close'] = '</span></li>';
		$config['prev_link'] = ' ← ';
		$config['next_link'] = ' → ';
        
        
        $this->pagination->initialize($config); 
        $data['links']=$this->pagination->create_links();
        
       
		$data['current_page']=$current_page;
        
        $data['properties'] = $this->property_model->properties($config['per_page'], $current_page, 'Requested');
        $this->load->view('admin/property/requested', $data);
    }
    
    //this function for active user
    public function active($property_id=null){
        if(!$property_id or !is_numeric($property_id))
            redirect('admin/property');
        if($this->db->where('id', $property_id)->update('properties', array('status'=>'Published'))){
            $this->session->set_userdata('success_msg', ' This property has been published.');
        }else{
            $this->session->set_userdata('error_msg', 'Something wrong please try again');
        }
        
        redirect('admin/property/requested');
    }
    
}
