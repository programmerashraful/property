<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller {
    
	public function index()
	{
		$session_attr=array(
            'current_user_id'=>'',
            'current_user_name'=>'',
            'current_user_type'=>'',
            'success_msg'=>'Log Out Success',
        );
        $this->session->set_userdata($session_attr);
        redirect('login');
	}
    
    
}
