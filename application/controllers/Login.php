<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    public function __construct(){
        parent:: __construct();
        $this->load->model('user_model');
        
        if($this->user_model->is_user_login()){
            redirect('admin/dashboard');
        }
        
    }
    
    
	public function index()
	{
        //get login data
        if($this->input->post('login')){
            $this->form_validation->set_rules('user_name', 'User Name', 'trim|required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            
            if($this->form_validation->run()){
                $user_name = $this->input->post('user_name');
                $password = $this->input->post('password');
                // do login process
                if($this->user_model->try_login($user_name, md5($password))){
                    $this->session->set_userdata('success_msg', 'Login Success');
                    if($redirect = $this->input->get('redirect')){
                        redirect($redirect);
                    }else{
                         redirect('admin/dashboard');
                    }
                   
                }else{
                    $this->session->set_userdata('error_msg', 'Login Failed. Username and password not match.');
                }
            }else{
                $errors = validation_errors('<p>', '</p>');
                $this->session->set_userdata('error_msg', $errors);
            }
        }
        
        
		$this->load->view('frontend/login');
	}
    
    
}
