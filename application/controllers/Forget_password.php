<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forget_password extends CI_Controller {
    public function __construct(){
        parent::__construct();
        if($this->user_model->is_user_login()){
            redirect('user/dashboard');
        }
         
    }
	public function index($error=null)
	{
        $data=array();
        if($this->input->post('recover_email')){
            $email = $this->input->post('email');
            $query = $this->db->get_where('users', array('email'=>$email));
            $result  = $query->result();
            
            if($result){
                $change_log=array();
                $this->load->helper('string');
                $key = random_string('alnum', 30);
                $change_log['email']=$email;
                $change_log['key']=$key;
                $this->session->set_userdata($change_log);
                
                
                //send email
                if($this->send_email($email, $key)){
                    $this->session->set_userdata('success_msg', 'Your recovery password link has been send to your email. Thank you.');
                }else{
                    $this->session->set_userdata('error_msg', 'Email sending problem please try again');
                }
                
            }else{
                $this->session->set_userdata('error_msg', 'This email is not in our records.');
            }
        }
		$this->load->view('frontend/forget_email', $data);
	}
    
    
    public function recover($key){
        if($key!=$this->session->userdata('key')){redirect('forget_password');}
        if($this->input->post('recover_password')){
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('confirm_password', 'Password', 'required|matches[password]');
            if($this->form_validation->run()){
                $email = $this->session->userdata('email');
                $attr['password'] = md5($this->input->post('password'));
                if($this->db->where('email', $email)->update('users', $attr)){
                    $this->session->set_userdata('success_msg', 'Your password has been changed');
                    $this->session->set_userdata('key', '');
                    redirect('login');
                }else{
                    $this->session->set_userdata('error_msg', 'recovery error');
                }
            }else{
                $this->session->set_userdata('error_msg', validation_errors('<p>', '</p>'));
            }
        }
        $this->load->view('frontend/recover_password');
    }
    
    
    
    //this function for send email 
    public function send_email($email=null, $key=null){
        $link = site_url('forget_password/recover/'.$key);
        $style = 'style="display:inline-block;background:green;border-radius:5px;color:yellow;padding:5px 10px"';
        $message = 'Please click the link bellow. <br /> <a href="'.$link.'" '.$style.'> Click to change password </a>';
        
        $this->load->library('email');

        $this->email->set_newline("\r\n");

        $config['protocol'] = 'smtp';
        $config['smtp_host'] = $this->user_model->get_setting_data('smtp_host');
        $config['smtp_port'] = $this->user_model->get_setting_data('smtp_port');
        $config['smtp_user'] = $this->user_model->get_setting_data('smtp_user');
        $config['smtp_from_name'] = $this->user_model->get_setting_data('site_title');
        $config['smtp_pass'] = $this->user_model->get_setting_data('smtp_pass');
        $config['wordwrap'] = TRUE;
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html';                       
        $config['smtp_crypto'] = 'ssl';                       

        $this->email->initialize($config);

        $this->email->from($config['smtp_user'], $config['smtp_from_name']);
        $this->email->to($email);
        $this->email->subject('PASSWORD RECOVERY');

        $this->email->message($message);
        return $this->email->send();
        
        
    }
    
   
}
