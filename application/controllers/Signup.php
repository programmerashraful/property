<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signup extends CI_Controller {
    
	public function index()
	{
        
        
        if($this->input->post('signup')){
            $this->form_validation->set_rules('account_type', 'Account Type', 'required|callback_usertype_check');
            $this->form_validation->set_rules('full_name', 'Full Name', 'required|trim');
            $this->form_validation->set_rules('user_name', 'User Name', 'required|is_unique[users.user_name]');
            $this->form_validation->set_rules('phone', 'Phone', 'required|is_unique[users.phone]|min_length[11]|max_length[11]');
            $this->form_validation->set_rules('email', 'Email', 'valid_email|is_unique[users.email]');
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');
            $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[password]');
            
            //if this form validate runn
            if($this->form_validation->run()){
                $attr = array(
                    'type'=>$this->input->post('account_type'),
                    'full_name'=>$this->input->post('full_name'),
                    'user_name'=>$this->input->post('user_name'),
                    'phone'=>$this->input->post('phone'),
                    'email'=>$this->input->post('email'),
                    'password'=>md5($this->input->post('password')),
                );
                if($this->db->insert('users', $attr)){
                    $this->session->set_userdata('success_msg', 'Your sign up successfull please login.');
                    redirect('login');
                }else{
                    $this->session->set_userdata('error_msg', 'Something wrong please try again');
                }
            }else{
                $errors = validation_errors('<p>', '</p>');
                $this->session->set_userdata('error_msg', $errors);
            }
        }        
		$this->load->view('frontend/signup');
	}
    
    
    
    
    public function usertype_check($str)
    {
        if ($str == 'admin'){
            $this->form_validation->set_message('username_check', 'The %s field can not be the word "admin"');
            return FALSE;
        }
        else{
            return TRUE;
        }
    }
    
    
}
