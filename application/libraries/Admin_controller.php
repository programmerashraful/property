<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_controller extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->user_type = $this->session->userdata('current_user_type');
        if(!$this->user_model->is_user_login()){
            redirect('login');
        }
        if($this->user_type!='admin'){
            redirect('users/dashboard');
        }
    }
    
}
