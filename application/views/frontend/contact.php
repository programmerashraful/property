<?php $this->load->view('frontend/header'); ?>
<section class="about_section">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <div class="section-header">
                    <h1>GET IN TOUCH</h1>
                    <p> For further query please contact with us. </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
               <div class="login_part contact_page">
                    <?php echo form_open('contact'); ?>
                      <div class="form-group">
                        <input name="person_name" type="text" class="form-control" placeholder="Enter Your Name" required>
                        <i class="fa fa-user"></i>
                      </div>
                      <div class="form-group">
                        <input name="email" type="email" class="form-control" placeholder="Enter Email" required>
                        <i class="fa fa-envelope-o"></i>
                      </div>
                      <div class="form-group">
                        <input name="subject" type="text" class="form-control"  placeholder="Enter Your Subject" required>
                        <i class="fa fa-edit"></i>
                      </div>
                      <div class="form-group">
                        <textarea name="message" class="form-control" cols="30" rows="10" placeholder="Type your messgae" required></textarea>
                        <i class="fa fa-comments"></i>
                      </div>
                      <button type="submit" name="send_message" value="submit" class="btn btn-primary">Submit Query</button>
                    <?php echo form_close(); ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="embed-responsive embed-responsive-4by3">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1809.0211769134514!2d91.971908443994!3d24.930627033399983!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x375053bc35c47f1b%3A0x9376ed298a3c57a9!2zTWV0cm9wb2xpdGFuIFVuaXZlcnNpdHksIFN5bGhldCAtIOCmruCnh-Cmn-CnjeCmsOCni-CmquCmsuCmv-Cmn-CmqCDgpofgpongpqjgpr_gpq3gpr7gprDgp43gprjgpr_gpp_gpr8sIOCmuOCmv-CmsuCnh-Cmnw!5e0!3m2!1sen!2sbd!4v1591872430480!5m2!1sen!2sbd" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div>
        </div>
    </div>
   
</section>
<?php $this->load->view('frontend/footer'); ?>