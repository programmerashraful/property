<?php $this->load->view('frontend/header'); ?>
   
<section class="about_section">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <div class="section-header">
                    <h1>About Us</h1>
                    <p>We are dealing with your care</p>
                </div>
            </div>
            <div class="col-sm-12 about-us-item">
                <div class="row">
                    <div class="col-md-4">
                        <div class="each-features-body yellow">
                            <div class="staff-item-wrapper">
                                <div class="logo-with-title">
                                    <i class="fa fa-line-chart"></i>
                                    <h2 class="title">Sale Property</h2>
                                    <hr class="hr">
                                    <p class=" detail">User will get crucial benefit from this website.They could checkout their choiceable house or apartment for sale with details information.The property owner could be access their specific data or information,when they want to added a property for rent or sale with multiple picture.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="each-features-body purple">
                            <div class="staff-item-wrapper">
                                <div class="logo-with-title">
                                    <i class="fa fa-magic"></i>
                                    <h2 class="title">Rent Property</h2>
                                    <hr class="hr">
                                    <p class="detail">people or user can find their choiceable house for rent.customers can easily see the location of this property by using the map link.property owner were able to add their property with multiple picture.It will be more helpful and easier for customer to choose an apartment or house among theirs  appetite.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="each-features-body navi-blue">
                            <div class="staff-item-wrapper">
                                <div class="logo-with-title">
                                    <i class="fa fa fa-trophy"></i>
                                    <h2 class="title">List Property</h2>
                                    <hr class="hr">
                                    <p class="detail">
                                         From this website you can see all the available property which is listed by owner.User can see all the details  about house or apartment.people are also contact by message,phone,email with property owner from this website.property owner could listed their property very easily and transfer their member amount by bkash transaction system.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 about-us-details">
                <p>
                    <b>About System:- </b>This website  made for rent and sale system with four categories of house or property and will be available for all division and district. It is a legal protection website for using as a user or property owner.This site provide security and verification system to add a property.So,no one could be able to access a fake or unvalid property.For more information,all of the owner could added their property after payment a selected of money.After that, admin will checkout the details and active the owner member.
                </p>
            </div>
        </div>
        <!-- End: .row .footer-body .section-separator  -->
    </div>
    <!-- End: .container  -->
</section>
    
   <?php $this->load->view('frontend/footer'); ?>