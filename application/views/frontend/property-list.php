<?php $this->load->view('frontend/header'); ?>
    
    <section class="breadcrumbs">
        <div class="container  mt-3  mb-3" id="app">
              <form action="<?php echo site_url('property/index'); ?>" method="get">
               <div class="row">
                   <div class="col-sm-12 search_box">
                       <div class="row">
                           
                       </div>
                       <div class="row">
                           <div class="col-sm-4"  v-if="divisions">
                            <div class="form-group">
                                <select name="division_id" class="form-control custom-select" id="division" v-model="division_id" @change="getDistricts()">
                                  <option value="">Division..</option>
                                  <option v-for="(division,key,index) in divisions" v-bind:value="division.id"> {{ division.name }} </option>
                                </select>
                              </div>
                           </div>
                           
                           <div  v-if="districts" class="col-sm-4">
                            <div class="form-group">
                                <select name="district_id" class="form-control custom-select" id="district" @change="getMedicals()" v-model="district_id">
                                  <option value="">Districts..</option>
                                  <option v-for="(district,key,index) in districts" v-bind:value="district.id"> {{ district.name }} </option>
                                </select>
                              </div>
                           </div>
                           
                           <div class="col-sm-4"  v-if="district_id">
                            <div class="form-group">
                                <select name="type" class="form-control custom-select" id="type" v-model="type">
                                  <option value="">Type..</option>
                                  <option value="Sale">For Sale</option>
                                  <option value="Rent">For Rent</option>
                                  <option value="Rent With Furniture">Rent With Furniture</option>
                                  <option value="Rent For Bachelor">Rent For Bachelor</option>
                                </select>
                              </div>
                           </div>
                           <div class="col-sm-12">
                               <button type="submit" class="btn btn-info btn-block btn-flat"> <i class="fa fa-search"></i> Search property </button>
                           </div>
                       </div>
                   </div>
               </div>
               </form>
            </div>
    </section>
    
    <section class="doctors_list">
        <div class="container">
           
           
           
            <?php 
                    $counter=0; $col_counter=0; if($properties){foreach($properties as $property){ 
                    $counter++;
                    $col_counter++;
                    
                    if($counter==1){echo '<div class="row">';}
                ?>
                <div class="col-sm-3">
                    <div class="single_property">
                        <div class="image">
                            <a title="<?php echo $property->title; ?>" href="<?php echo site_url('property/view/'.$property->id); ?>"><img src="<?php echo site_url('uploads/property/'.$property->image); ?>" alt="<?php echo $property->title; ?>" class="img-fluid" style="width:100%"></a>
                        </div>
                        <div class="property_footer">
                            <div class="info text-center">
                                <h4><?php echo $property->title; ?></h4>
                                <p>For: <?php echo $property->type;  ?></p>
                            </div>
                            <div class="ratings text-center">
                              <div class="ratings text-center">
                                   <?php if($property->rating){ for($i=1; $i<=$property->rating; $i++){  ?>
                                        <i class="fa fa-star"></i>
                                    <?php }}else{ ?>
                                    
                                    <?php echo 'No rating'; } ?>
                                    
                                </div>
                            </div>
                            <div class="appoinment">
                                <a href="<?php echo site_url('property/view/'.$property->id); ?>" class="btn btn-link btn-sm"><?php echo '$'.$property->price ?></a>
                                <a href="<?php echo site_url('property/view/'.$property->id); ?>" class="btn btn-link btn-sm">Show Details</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php 
                        
                    if($counter==4){ echo '</div>'; $counter=0; }
                    if($counter!=0 and $col_counter == count($properties)){
                        echo '</div>'; 
                        
                    }
                }}else{ ?>
               
                
                <div class="row">
                    <div class="col-sm-12 mt-2  mb-3 white_block">
                        <h1 class="color-danger text-center">No property  found</h1>
                    </div>
                </div>
                <?php } ?>
                
                <?php if(isset($links) and $links){ ?>
                <div class="row">
                    <div class="col-sm-12 mt-3">
                        <?php echo $links; ?>
                    </div>
                </div>
                <?php } ?>
           
    
        </div>
    </section>
<?php 
$data=array();
if($division_id = $this->input->get('division_id'))
    $data['division_id']=$division_id;

if($district_id = $this->input->get('district_id'))
    $data['district_id']=$district_id;

$this->load->view('frontend/footer', $data); 
?>