<?php $this->load->view('frontend/header'); ?>


<section class="property_details">
   <?php if($owner){ ?>
    <section class="property_owner_section">
        <div class="container white_block" >
            <div class="row">
                <div class="col-sm-2 " style="border-right:1px solid #DDD">
                    <a href="<?php echo site_url('profile/owner/'. $owner->id); ?>" class="owner_name_image">
                        <img style="max-width:100px" src="<?php echo site_url('uploads/users/'.$owner->image); ?>" alt="" class="img-fluid rounded-circle img-thumbnail">
                        
                    </a>
                </div>
                <div class="col-sm-8">
                   
                </div>
                <div class="col-sm-2 pt-3">
                    <a href="<?php echo site_url('profile/owner_property/?owner='. $owner->id); ?>" class="btn btn-success btn-block"> <i class="fa fa-home"></i> View Property</a>
                    <p style="color:#00a3c8;text-transform: uppercase;font-weight: bold;text-align: center;margin: 10px 0px;"> <i class="fa fa-tag"></i> Property <span class="badge badge-warning"><?php echo $total_property; ?></span></p>
                </div>
            </div>
        </div>
    </section>
    <?php } ?>
    
    <section class="property_details pb-3" style="min-height:500px">
        <div class="container">
           
            <div class="row mb-2">
                <div class="col-sm-6 white_block" style="border-radius: 5px 0px 0px 5px;">
                    <h4 style="margin:0;padding:0"><i class="fa fa-user"></i> <?php echo $owner->full_name; ?></h4>
                </div>
                <div class="col-sm-6 white_block" style="border-radius: 0px 5px 5px 0px;">
                    <h4 style="margin:0;padding:0"><i class="fa fa-eye"></i><b> Gender:-</b> <?php echo $owner->gender; ?></h4>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-12 white_block">
                    <div class="row mb-2">
                        <div class="col-sm-4" style="border-right: 1px solid #DDD;">
                            <div class="property_block">
                                <i class="fa fa-location-arrow"></i>
                                <a href=""><?php echo $owner->address; ?></a>
                            </div>
                        </div>
                        <div class="col-sm-4" style="border-right: 1px solid #DDD;">
                            <div class="property_block">
                                <i class="fa fa-phone"></i>
                                <a href="tel:<?php echo $owner->phone; ?>"><?php echo $owner->phone; ?></a>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="property_block">
                                <i class="fa fa-envelope-o"></i>
                                <a href="mailto:<?php echo $owner->email; ?>"><?php echo $owner->email;  ?></a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="seperator">
                            
                        </div>
                    </div>
                    
                    <div class="row mt-2" style="color:#28A745">
                        <div class="col-sm-4">
                            <p><b><i class="fa fa-tag"></i> Country:- </b> <?php echo $owner->country; ?></p>
                        </div>
                        <div class="col-sm-4">
                            <p><b><i class="fa fa-tag"></i> City:- </b> <?php echo $owner->city; ?></p>
                        </div>
                        <div class="col-sm-4">
                            <p><b><i class="fa fa-tag"></i> State:- </b> <?php echo $owner->state; ?></p>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="seperator">
                            
                        </div>
                    </div>
                    
                    <div class="row mt-2" style="color:#28A745">
                        <div class="col-sm-4">
                            <p><b><i class="fa fa-tag"></i> NID Number:- </b> <?php echo $owner->nid_number; ?></p>
                        </div>
                        <div class="col-sm-4">
                            <p><b><i class="fa fa-tag"></i> Post Code:- </b> <?php echo $owner->post_code; ?></p>
                        </div>
                        <div class="col-sm-4">
                            <p><b><i class="fa fa-tag"></i> Gender:- </b> <?php echo $owner->gender; ?></p>
                        </div>
                    </div>
                </div>
            </div>
            
            
            <div class="row mt-2">
                <div class="col-sm-12 white_block">
                    <h4 style="font-weight:400" ><i class="fa fa-file-text"></i> About Owner <b><?php echo $owner->full_name; ?></b></h4>
                    <p><?php echo $owner->about; ?></p>
                </div>
            </div>
            
           
            
        </div>
    </section>
    
   
</section>


<?php $this->load->view('frontend/footer'); ?>