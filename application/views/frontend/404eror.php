<?php $this->load->view('frontend/header'); ?>
    

   <section class="login_section">
       <div class="container">
           <div class="row">
               <div class="col-sm-10 offset-sm-1">
                   <div class="login_template">
                       <div class="row">
                           <div class="col-sm-12 welcome_part text-center" style="border-radius:15px">
                               <h1>404 ERROR</h1>
                               <p>We can not found your requested url</p>
                               <a href="<?php echo site_url(); ?>" class="btn btn-default">Back to home</a>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </section> 
    <?php $this->load->view('frontend/footer'); ?>