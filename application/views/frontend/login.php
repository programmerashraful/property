<?php $this->load->view('frontend/header'); 
$redirect = $this->input->get('redirect');
if($redirect){$redirect = '/?redirect='.$redirect;}
?>
    

   <section class="login_section">
       <div class="container">
           <div class="row">
               <div class="col-sm-10 offset-sm-1">
                   <div class="login_template">
                       <div class="row">
                           <div class="col-sm-4 welcome_part text-center">
                               <h1>Welocme Back</h1>
                               <p>If you don't have an account please click here</p>
                               <a href="<?php echo site_url('signup'); ?>" class="btn btn-default">Sign Up</a>
                           </div>
                           <div class="col-sm-8 login_part">
                                <div class="row">
                                    <div class="col-sm-8 offset-sm-2">
                                        <h1> Log In</h1>
                                       <?php echo  form_open('login'.$redirect); ?>
                                          <div class="form-group">
                                            <input type="text" name="user_name" class="form-control"  aria-describedby="emailHelp" placeholder="User Name" required >
                                            <i class="fa fa-user"></i>
                                          </div>
                                          <div class="form-group">
                                            <input type="password" name="password" class="form-control" placeholder="Password" required>
                                            <i class="fa fa-lock"></i>
                                          </div>
                                          <button type="submit" name="login" value="Login" class="btn btn-primary">Submit</button> <a style="border: none;color:#959595;" href="<?php echo site_url('forget_password'); ?>" class="btn btn-link">Forget Password</a>
                                        <?php echo form_close(); ?>
                                    </div>
                                </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </section> 
    <?php $this->load->view('frontend/footer'); ?>