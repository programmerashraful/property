    <?php $this->load->view('frontend/header'); ?>
    <section class="login_section">
       <div class="container">
           <div class="row">
               <div class="col-sm-10 offset-sm-1">
                   <div class="login_template">
                       <div class="row">
                           <div class="col-sm-4 welcome_part text-center">
                               <h1>Welocme Back</h1>
                               <p>If you don't have an account please click here</p>
                               <a href="<?php echo site_url('signup'); ?>" class="btn btn-default">Sign Up</a>
                           </div>
                           <div class="col-sm-8 login_part">
                                <div class="row">
                                    <div class="col-sm-8 offset-sm-2">
                                        <h1>Reset Password </h1>
                                       <?php echo  form_open('forget_password/recover/'.$this->session->userdata('key')); ?>
                                          <div class="form-group">
                                            <input type="password" name="password" class="form-control"  aria-describedby="emailHelp" placeholder="Type your new password" value="<?php echo $this->input->post('password'); ?>" required >
                                            <i class="fa fa fa-lock"></i>
                                          </div>
                                          <div class="form-group">
                                            <input type="password" name="confirm_password" class="form-control"  aria-describedby="emailHelp" value="<?php echo $this->input->post('confirm_password'); ?>" placeholder="Type confirm Password" required >
                                            <i class="fa fa fa-lock"></i>
                                          </div>
                                          <button type="submit" name="recover_password" value="recover_password" class="btn btn-primary">Submit</button> 
                                        <?php echo form_close(); ?>
                                    </div>
                                </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </section> 
    <?php $this->load->view('frontend/footer'); ?>