<?php $this->load->view('frontend/header'); ?>


<section class="property_details">
  
   <?php if($owner and $this->user_model->is_user_login()){ ?>
    <section class="property_owner_section">
        <div class="container white_block" >
            <div class="row">
                <div class="col-sm-3 text-center"  style="border-right: 1px solid #DDD;">
                    <a href="<?php echo site_url('profile/owner/'. $owner->id); ?>" class="owner_name_image">
                        <img style="max-width:80px" src="<?php echo site_url('uploads/users/'.$owner->image); ?>" alt="" class="img-fluid rounded-circle img-thumbnail">
                        <p><?php echo $owner->full_name; ?></p>
                    </a>
                </div>
                <div class="col-sm-7"  style="border-right: 1px solid #DDD;">
                    <ul class="owner_contacts">
                        <li><a href="tel:<?php echo $owner->phone; ?>"> <i class="fa fa-phone"></i> <?php echo $owner->phone; ?> </a></li>
                        <li><a href="mailto:<?php echo $owner->email; ?>"> <i class="fa fa-envelope-o"></i> <?php echo $owner->email; ?></a></li>
                        <li><a href="#"> <i class="fa fa-location-arrow"></i> <?php echo $owner->address; ?></a></li>
                    </ul>
                </div>
                <div class="col-sm-2 ">
                    <a href="<?php echo site_url('profile/owner_property/?owner='. $owner->id); ?>" class="btn btn-success btn-block"> <i class="fa fa-home"></i> View Property</a>
                    
                    <a href="<?php echo site_url('profile/owner/'. $owner->id); ?>" class="btn btn-success btn-block"> <i class="fa fa-user"></i> View Profile</a>
                    
                    <a href="<?php echo site_url('users/messenger/'. $owner->id); ?>" class="btn btn-success btn-block"> <i class="fa fa-envelope-o"></i> Contact</a>
                </div>
            </div>
        </div>
    </section>
    <?php } ?>
    
    <section class="property_details pb-3 mt-2">
        <div class="container">
           
            <div class="row mb-2">
                <div class="col-sm-12 white_block">
                    <h4 style="margin:0;padding:0"><i class="fa fa-map-marker"></i> <?php echo $property->title; ?></h4>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-12 white_block">
                    <div class="row mb-2">
                       <?php if($this->user_model->is_user_login()){ ?>
                        <div class="col-sm-3" style="border-right: 1px solid #DDD;">
                            <div class="property_block">
                                <i class="fa fa-location-arrow"></i>
                                <a href=""><?php echo $property->address; ?></a>
                            </div>
                        </div>
                        <div class="col-sm-3" style="border-right: 1px solid #DDD;">
                            <div class="property_block">
                                <i class="fa fa-phone"></i>
                                <a href="tel:<?php echo $property->phone; ?>"><?php echo $property->phone; ?></a>
                            </div>
                        </div>
                        <div class="col-sm-3" style="border-right: 1px solid #DDD;">
                            <div class="property_block">
                                <i class="fa fa-envelope-o"></i>
                                <a href="mailto:<?php echo $property->email; ?>"><?php echo $property->email;  ?></a>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="col-sm-<?php if($this->user_model->is_user_login()){echo '3';}else{echo '12';} ?>">
                            <div class="property_block">
                                <i class="fa fa-tag"></i>
                                <a href=""><?php echo $property->type; ?> $<?php echo $property->price; if($property->rent_type){echo ' ('.$property->rent_type.')';} ?></a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="seperator">
                            
                        </div>
                    </div>
                    <div class="row mt-2" style="color:#28A745">
                        <div class="col-sm-4">
                            <p><b><i class="fa fa-tag"></i> Bed Room:- </b> <?php echo $property->bed_room; ?></p>
                            <p><b><i class="fa fa-tag"></i> Bath Room:- </b> <?php echo $property->bath_room; ?></p>
                            
                        </div>
                        <div class="col-sm-4">
                            <p><b><i class="fa fa-tag"></i> Drowing Room:- </b> <?php echo $property->dining_room; ?></p>
                            <p><b><i class="fa fa-tag"></i> Lift:- </b> <?php echo $property->lift; ?></p>
                        </div>
                        <div class="col-sm-4">
                            <p><b><i class="fa fa-tag"></i> Garage:- </b> <?php echo $property->garage; ?></p>
                        </div>
                    </div>
                </div>
            </div>
            
           
            <div class="row mt-2">
                <div class="col-sm-12 white_block">
                    <h4 ><i class="fa fa-file-text"></i> Description</h4>
                    <p><?php echo $property->description; ?></p>
                </div>
            </div>
            
            
            
            <div class="row mt-2">
                <div class="col-sm-12 white_block">
                    <h4 style="font-weight:400" ><i class="fa fa-photo"></i> Pictures <b></b></h4>
                    <div class="row mb-3">
                        <div class="seperator"></div>
                    </div>
                    <ul class="row" id="lightgallery" style="list-style:none;margin:0;padding:0">
                       <?php $pictures = json_decode($property->property_images); if($pictures){ foreach($pictures as $item){ ?>
                        <li class="col-sm-2" data-responsive="<?php echo site_url('uploads/property/'.$item); ?>" data-src="<?php echo site_url('uploads/property/'.$item); ?>">
                            <div class="property_pictures mb-3" >
                               <a data-lightbox="roadtrip" href="<?php echo site_url('uploads/property/'.$item); ?>">
                                    <img src="<?php echo site_url('uploads/property/'.$item); ?>" alt="" class="img-fluid  img-thumbnail" style="width:100%;height: 90px;">
                               </a>
                            </div>
                        </li>
                        <?php }} ?>
                        
                    </ul>
                </div>
            </div>
           
            <div class="row mt-2">
               
                <div class="col-sm-6 white_block">
                    <h4 ><i class="fa fa-map-marker"></i> Google Map Location</h4>

                    <div class="row mb-3">
                        <div class="seperator"></div>
                    </div>
                    
                    <div class="embed-responsive embed-responsive-16by9">
                     <?php echo $property->google_location; ?>
                    </div>
                    
                </div>
                
                <div class="col-sm-6 white_block">
                    <h4 ><i class="fa fa-youtube"></i> Youtube Video </h4>
                    
                    <div class="row mb-3">
                        <div class="seperator"></div>
                    </div>
                    
                    <div class="embed-responsive embed-responsive-16by9">
                     <?php if($property->youtube_video){ ?>
                      <iframe width="560" height="315" src="<?php echo $property->youtube_video; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                      <?php } ?>
                    </div>
                    
                </div>
                
            </div>
            
        </div>
    </section>
</section>

<?php $this->load->view('frontend/footer'); ?>