<?php $this->load->view('frontend/header'); ?>


<section class="property_details">
   <?php if($owners){ foreach($owners as $owner){ ?>
    <section class="property_owner_section">
        <div class="container white_block" >
            <div class="row">
                <div class="col-sm-3 text-center">
                    <a href="<?php echo site_url('profile/owner/'. $owner->id); ?>" class="owner_name_image">
                        <img style="max-width:80px" src="<?php echo site_url('uploads/users/'.$owner->image); ?>" alt="" class="img-fluid rounded-circle img-thumbnail">
                        <p><?php echo $owner->full_name; ?></p>
                    </a>
                </div>
                <div class="col-sm-7">
                    <ul class="owner_contacts">
                        <li><a href="tel:<?php echo $owner->phone; ?>"> <i class="fa fa-phone"></i> <?php echo $owner->phone; ?> </a></li>
                        <li><a href="mailto:<?php echo $owner->email; ?>"> <i class="fa fa-envelope-o"></i> <?php echo $owner->email; ?></a></li>
                        <li><a href="#"> <i class="fa fa-location-arrow"></i> <?php echo $owner->address; ?></a></li>
                    </ul>
                </div>
                <div class="col-sm-2 ">
                    <a href="<?php echo site_url('profile/owner_property/?owner='. $owner->id); ?>" class="btn btn-success btn-block"> <i class="fa fa-home"></i> View Property</a>
                    <a href="<?php echo site_url('profile/owner/'. $owner->id); ?>" class="btn btn-success btn-block"> <i class="fa fa-user"></i> View Profile</a>
                    
                    <a href="<?php echo site_url('users/messenger/'. $owner->id); ?>" class="btn btn-success btn-block"> <i class="fa fa-envelope-o"></i> Contact</a>
                </div>
            </div>
        </div>
    </section>
    <?php }} ?>
    
    <section class="links">
        <div class="container">
            
            <?php if(isset($links) and $links){ ?>
            <div class="row">
                <div class="col-sm-12 mt-3">
                    <?php echo $links; ?>
                </div>
            </div>
            <?php } ?>
        </div>
    </section>
    
</section>


<?php $this->load->view('frontend/footer'); ?>