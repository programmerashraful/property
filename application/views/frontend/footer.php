
    <!--This markup for footer-->
        <!--This markup for footer-->
    <section class="footer_section">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="widget">
                        <p><i class="fa fa-location-arrow"> </i> Batteshor, Sylhet, Bangladesh </p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="widget">
                        <p><i class="fa fa-phone-square "> </i> +880-1784381304</p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="widget">
                        <p><i class="fa fa-envelope-o"> </i> saimashahrin2@gmail.com</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="footer_copyright">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                   <p> &copy; Alright Reserved by Saima Shahrin</p>
                </div>
                <div class="col-sm-6">
                    <div class="widget footer_social">
                        <a class="afacebook" href="https://fb.com/"><i class="fa fa-facebook"></i></a>
                        <a class="atwitter" href="https://twitter.com/"><i class="fa fa-twitter"></i></a>
                        <a class="ainstagram" href="https://fb.com/"><i class="fa fa-instagram"></i></a>
                        <a class="alinkedin" href="https://fb.com/"><i class="fa fa-linkedin"></i></a>
                        <a class="ayoutube" href="https://fb.com/"><i class="fa fa-youtube"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <script src="<?php echo  site_url('assets/frontend/js/jquery-3.3.1.slim.min.js'); ?>"></script>
    <script src="<?php  echo site_url('assets/frontend/js/popper.min.js'); ?>"></script>
    <script src="<?php echo  site_url('assets/frontend/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo  site_url('assets/frontend/js/lightbox.min.js'); ?>"></script>
    <script src="<?php echo site_url('assets/admin/'); ?>plugins/select2/select2.full.min.js"></script>
    <!-- datepicker -->
    <script src="<?php echo site_url('assets/admin/'); ?>plugins/datepicker/bootstrap-datepicker.js"></script>
    <script>
      $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
      });
    </script>
    <script src="<?php echo site_url('assets/admin/'); ?>plugins/select2/select2.full.min.js"></script>
<script>
        $('#checkindate').datepicker({format: 'dd/mm/yyyy'});
		$('#checkoutdate').datepicker({format: 'dd/mm/yyyy'});
		$('#birthday').datepicker({format: 'dd/mm/yyyy'});
        $(function () {
          $('[data-toggle="tooltip"]').tooltip()
        });
    
    
    
</script>
   
   
   
   <script src="<?php echo site_url('assets/frontend/js/form-validator/jquery.form-validator.min.js'); ?>"></script>
    <script src="<?php echo site_url('assets/frontend/js/form-validator/security.js'); ?>"></script>
    
    <script>

   
   $(document).ready(function(){
       
       
       $('#phone').on('change keyup', function() {
      // Remove invalid characters
          var sanitized = $(this).val().replace(/[^0-9]/g, '');
          // Update value
          $(this).val(sanitized);
        });
    
        $('#full_name').on('change keyup', function() {
          // Remove invalid characters
          var sanitized = $(this).val().replace(/[0-9`~!@#$%^&*()_|+\-=?;:'",<>\{\}\[\]\\\/]/gi,'');
          // Update value
          $(this).val(sanitized);
        });
    
       
       
       $.validate({
            modules : 'location, date, security, file',
            onModulesLoaded : function() {
              $('#country').suggestCountry();
            }
          });
       
       
       jQuery.validator.addMethod("lettersonly", function(value, element) 
        {
        return this.optional(element) || /^[a-z ]+$/i.test(value);
        }, "Letters and spaces only please");
       
       
   });
        

    </script>
   
   
   
   
   
   
    <script src="<?php echo  site_url('assets/admin/dist/js/vue.js'); ?>"></script>
    <script src="<?php echo  site_url('assets/admin/dist/js/axios.min.js'); ?>"></script>
    <script>
    var app = new Vue({
          el: '#app',
          data: {
            ajax_url: '<?php echo site_url('ajax'); ?>',
            user_url: '<?php echo site_url('users'); ?>',
            divisions:null,
            file_upload:'',
            property_gallery:null,
            property_images:<?php if(isset($property_images) and $property_images){echo $property_images;}else{echo '[]';} ?>,
            imageUploading:false,
            imageUploadError:false,
            type:"",
            districts:null,
            division_id:<?php if(isset($division_id) and $division_id){echo '"'.$division_id.'"';}else{echo '""';} ?>,
            district_id:<?php if(isset($district_id) and $district_id){echo '"'.$district_id.'"';}else{echo '""';} ?>,

          },

            created(){
                this.getDivisions();
                if(this.division_id!=""){
                    this.getDistricts();
                }
            },


            methods:{

                // send message to account customer
                getDivisions(){
                     axios.get(this.ajax_url+'/get_divisions')
                    .then(res => {
                        this.divisions = res.data;
                    })
                    .catch(err => {console.log(err); });
                    
                },
                
                
                //this method for get districts
                getDistricts(){
                    if(this.division_id==""){
                   }else{
                        axios.get(this.ajax_url+'/get_districts/'+this.division_id)
                        .then(res => {
                            this.districts = res.data;
                        })
                        .catch(err => {console.log(err); });
                      
                   }
                    
                },
                
                
                //upload image
                uploadImage(){
                    this.imageUploading=true;
                    this.file_upload = this.$refs.file_upload.files[0];
                    
                    
                    let formData = new FormData();
                    formData.append('property_gallery', this.file_upload);
                    axios.post(this.user_url+'/ajax_image_upload',
                    formData,
                    {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }

                      }
                    ).then(res => {
                        this.imageUploading=false;
                        if(res.data){
                            if(res.data.error==false){
                                let ImageItem = res.data.file;
                                this.property_images.push(ImageItem);
                                this.imageUploadError=false;
                            }else{
                                this.imageUploadError=true;
                            }
                        }else{
                            this.imageUploadError=true;
                        }
                    })
                    .catch(error => {
                        this.imageUploadError=true;
                        this.imageUploading=false;
                        console.log('FAILURE!!');
                    });

              },
                
                //this function for remove property images
                removePropertyImage(itemIndex) {
                   this.property_images.splice(itemIndex, 1);
                },
                

            }

        });

    </script>
    
    
    
</body>
</html>