<?php $this->load->view('frontend/header'); ?>
   <section class="login_section">
      
       <div class="container">
           <div class="row">
               <div class="col-sm-10 offset-sm-1">
                   <div class="login_template">
                       <div class="row">
                           <div class="col-sm-4 welcome_part text-center">
                               <h1>Welocme Back</h1>
                               <p>If you have an account  please click the button bellow</p>
                               <a href="<?php echo site_url('login'); ?>" class="btn btn-default">Log In</a>
                           </div>
                           <div class="col-sm-8 login_part sign_up_part">
                                <div class="row">
                                    <div class="col-sm-8 offset-sm-2">
                                        <h1> Sign Up </h1>
                                       <?php echo form_open('signup'); ?>
                                          <div class="form-group">
                                            <select class="form-control custom-select" name="account_type" id="" required>
                                                <option value=""> Property Owner / General Member (Select Account Type)</option>
                                                <option value="owner" <?php if($this->input->post('account_type')=='owner'){echo 'selected';} ?> >Owner</option>
                                                <option value="member" <?php if($this->input->post('account_type')=='member'){echo 'selected';} ?>>General Member</option>
                                            </select>
                                            <i class="fa fa-plus"></i>
                                          </div>
                                          <div class="form-group">
                                            <input type="text" name="full_name" class="form-control"  aria-describedby="emailHelp" placeholder="Full Name" value="<?php echo $this->input->post('full_name'); ?>" data-validation-allowing="-_ " data-validation="required lettersonly"  data-validation-ignore="1234567890" id="full_name" required>
                                            <i class="fa fa-user"></i>
                                          </div>
                                          <div class="form-group">
                                            <input type="text" name="user_name" class="form-control"  aria-describedby="emailHelp" placeholder="User name" value="<?php echo $this->input->post('user_name'); ?>"  data-validation="alphanumeric" data-validation-allowing="-_" required>
                                            <i class="fa fa-user"></i>
                                          </div>
                                          <div class="form-group">
                                            <input maxlength="11" type="text" name="phone" class="form-control"  aria-describedby="emailHelp" placeholder="Phone" value="<?php echo $this->input->post('phone'); ?>" data-validation="length number required" data-validation-error-msg="Enter A valid phone number" data-validation-length="11-11" id="phone" required>
                                            <i class="fa fa-phone"></i>
                                          </div>
                                          <div class="form-group">
                                            <input type="email" name="email" class="form-control"  aria-describedby="emailHelp" placeholder="Enter email" value="<?php echo $this->input->post('email'); ?>" data-validation="email" data-validation-error-msg="Enter a valid email"  required>
                                            <i class="fa fa-envelope-o"></i>
                                          </div>
                                          <div class="form-group">
                                            <input name="password" id="password" type="password" class="form-control" placeholder="Password" value="<?php echo $this->input->post('password'); ?>"  data-validation="length strength required" data-validation-length="min8" data-validation-strength="2" data-validation-error-msg="Password Not Strong. Please input password minimum 8 length including numbers and chracters"  required>
                                            <i class="fa fa-lock"></i>
                                          </div>
                                          <div class="form-group">
                                            <input name="confirm_password" type="password" class="form-control" placeholder="Confirm Password" value="<?php echo $this->input->post('confirm_password'); ?>"  data-validation="confirmation required" data-validation-confirm="password" data-validation-error-msg="Confirm password does not match" required>
                                            <i class="fa fa-lock"></i>
                                          </div>
                                          
                                          <button type="submit" name="signup" value="signup" class="btn btn-primary">Submit</button>
                                        <?php echo form_close(); ?>
                                    </div>
                                </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </section> 
     <?php $this->load->view('frontend/footer'); ?>