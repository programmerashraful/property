    <?php $this->load->view('frontend/header', array('add_title'=>'User Login')); ?>
    
   <section class="login_section">
       <div class="container">
           <div class="row">
               <div class="col-sm-10 offset-sm-1">
                   <div class="login_template">
                       <div class="row">
                           <div class="col-sm-4 welcome_part text-center">
                               <h1>Welocme Back</h1>
                               <p>If you don't have an account please click here</p>
                               <a href="<?php echo site_url('signup'); ?>" class="btn btn-default">Sign Up</a>
                           </div>
                           <div class="col-sm-8 login_part">
                                <div class="row">
                                    <div class="col-sm-8 offset-sm-2">
                                        <h1>Do you forget your password ? </h1>
                                       <?php echo  form_open('forget_password'); ?>
                                          <div class="form-group">
                                            <input type="email" name="email" class="form-control"  aria-describedby="emailHelp" placeholder="Type your user email" required >
                                            <i class="fa fa fa-envelope-o"></i>
                                          </div>
                                          <button type="submit" name="recover_email" value="recover_email" class="btn btn-primary">Send Password Recovery Link</button> 
                                        <?php echo form_close(); ?>
                                    </div>
                                </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </section> 

    <?php $this->load->view('frontend/footer'); ?>