<?php $this->load->view('frontend/header'); ?>


<section class="property_details">
   <?php if($owner){ ?>
    <section class="property_owner_section">
        <div class="container white_block" >
            <div class="row">
                <div class="col-sm-3 text-center">
                    <a href="<?php echo site_url('profile/owner/'. $owner->id); ?>" class="owner_name_image">
                        <img style="max-width:80px" src="<?php echo site_url('uploads/users/'.$owner->image); ?>" alt="" class="img-fluid rounded-circle img-thumbnail">
                        <p><?php echo $owner->full_name; ?></p>
                    </a>
                </div>
                <div class="col-sm-7">
                    <ul class="owner_contacts">
                        <li><a href="tel:<?php echo $owner->phone; ?>"> <i class="fa fa-phone"></i> <?php echo $owner->phone; ?> </a></li>
                        <li><a href="mailto:<?php echo $owner->email; ?>"> <i class="fa fa-envelope-o"></i> <?php echo $owner->email; ?></a></li>
                        <li><a href="#"> <i class="fa fa-location-arrow"></i> <?php echo $owner->address; ?></a></li>
                    </ul>
                </div>
                <div class="col-sm-2 pt-3">
                    <a href="<?php echo site_url('profile/owner_property/'. $owner->id); ?>" class="btn btn-success btn-block"> <i class="fa fa-home"></i> View Property</a>
                    <a href="<?php echo site_url('profile/owner/'. $owner->id); ?>" class="btn btn-success btn-block"> <i class="fa fa-user"></i> View Profile</a>
                </div>
            </div>
        </div>
    </section>
    <?php } ?>
    
    
    <section class="doctors_list">
        <div class="container">
           
           
           
            <?php 
                    $counter=0; $col_counter=0; if($properties){foreach($properties as $property){ 
                    $counter++;
                    $col_counter++;
                    
                    if($counter==1){echo '<div class="row">';}
                ?>
                <div class="col-sm-3">
                    <div class="single_property">
                        <div class="image">
                            <a title="<?php echo $property->title; ?>" href="<?php echo site_url('property/view/'.$property->id); ?>"><img src="<?php echo site_url('uploads/property/'.$property->image); ?>" alt="<?php echo $property->title; ?>" class="img-fluid" style="width:100%"></a>
                        </div>
                        <div class="property_footer">
                            <div class="info text-center">
                                <h4><?php echo $property->title; ?></h4>
                                <p>For: <?php echo $property->type;  ?></p>
                            </div>
                            <div class="ratings text-center">
                              <div class="ratings text-center">
                                   <?php if($property->rating){ for($i=1; $i<=$property->rating; $i++){  ?>
                                        <i class="fa fa-star"></i>
                                    <?php }}else{ ?>
                                    
                                    <?php echo 'No rating'; } ?>
                                    
                                </div>
                            </div>
                            <div class="appoinment">
                                <a href="<?php echo site_url('property/view/'.$property->id); ?>" class="btn btn-link btn-sm"><?php echo '$'.$property->price ?></a>
                                <a href="<?php echo site_url('property/view/'.$property->id); ?>" class="btn btn-link btn-sm">Show Details</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php 
                        
                    if($counter==4){ echo '</div>'; $counter=0; }
                    if($counter!=0 and $col_counter == count($properties)){
                        echo '</div>'; 
                        
                    }
                }}else{ ?>
               
                
                <div class="row">
                    <div class="col-sm-12 mt-2 mb-3 white_block">
                        <h1 class="color-danger text-center">No property  found</h1>
                    </div>
                </div>
                <?php } ?>
                
                <?php if(isset($links) and $links){ ?>
                <div class="row">
                    <div class="col-sm-12 mt-3">
                        <?php echo $links; ?>
                    </div>
                </div>
                <?php } ?>
           
    
        </div>
    </section>
    
</section>


<?php $this->load->view('frontend/footer'); ?>