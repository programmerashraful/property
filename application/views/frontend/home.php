<?php $this->load->view('frontend/header'); ?>
    
    <section class="slider_section">
        
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 padding0">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                      <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                      </ol>
                      <div class="carousel-inner">
                        <div class="carousel-item active">
                          <img src="<?php echo site_url(); ?>/assets/frontend/images/slider1.jpg" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                          <img src="<?php echo site_url(); ?>/assets/frontend/images/slider2.jpg" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                          <img src="<?php echo site_url(); ?>/assets/frontend/images/slider3.jpg" class="d-block w-100" alt="...">
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
        
        <section class="search_section" id="app">
           <input type="hidden" v-model="ajax_url" name="ajax_url" value="<?php echo site_url('ajax'); ?>">
           
           
            <div class="container">
              <form action="<?php echo site_url('property/index'); ?>" method="get">
               <div class="row">
                   <div class="col-sm-6 offset-sm-3 search_box pb-3">
                       <div class="row">
                           <div class="col-sm-12 text-center"><h1>Search property !</h1></div>
                       </div>
                       <div class="row">
                           <div class="col-sm-12"  v-if="divisions">
                            <div class="form-group">
                                <select name="division_id" class="form-control custom-select" id="division" v-model="division_id" @change="getDistricts()" required>
                                  <option value="">Division..</option>
                                  <option v-for="(division,key,index) in divisions" v-bind:value="division.id"> {{ division.name }} </option>
                                </select>
                              </div>
                           </div>
                           
                           <div  v-if="districts" class="col-sm-12">
                            <div class="form-group">
                                <select name="district_id" class="form-control custom-select" id="district" v-model="district_id" required>
                                  <option value="">Districts..</option>
                                  <option v-for="(district,key,index) in districts" v-bind:value="district.id"> {{ district.name }} </option>
                                </select>
                              </div>
                           </div>
                           
                           <div class="col-sm-12"  v-if="district_id">
                            <div class="form-group">
                                <select name="type" class="form-control custom-select" id="type" v-model="type">
                                  <option value="">Type..</option>
                                  <option value="Sale">For Sale</option>
                                  <option value="Rent">For Rent</option>
                                  <option value="Rent With Furniture">Rent With Furniture</option>
                                  <option value="Rent For Bachelor">Rent For Bachelor</option>
                                </select>
                              </div>
                           </div>
                           <div class="col-sm-12">
                               <button type="submit" class="btn btn-info btn-block btn-flat"> <i class="fa fa-search"></i> Search property </button>
                           </div>
                       </div>
                   </div>
               </div>
               </form>
            </div>
        </section>
    </section>
    
    
    <section class="populer_medical_section"></section>
    <section class="populer_doctors_section"></section>
    
    
    <?php $this->load->view('frontend/footer'); ?>
    