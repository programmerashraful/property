<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!--Use this meta tag For responsive issue-->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="<?php echo site_url(); ?>/assets/frontend/images/favicon.ico">
    <title>Property Rent And Sale </title>
    <link rel="stylesheet" href="<?php echo site_url('assets/frontend/font-awesome/css/font-awesome.min.css'); ?>">
    
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo site_url('assets/admin/'); ?>plugins/select2/select2.min.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo site_url('assets/admin/'); ?>plugins/datepicker/datepicker3.css">
   
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900i&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo site_url('assets/frontend/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo site_url('assets/frontend/css/lightbox.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo site_url('assets/frontend/css/main.css'); ?>">
    <link rel="stylesheet" href="<?php echo site_url('assets/frontend/css/responsive.css'); ?>">
</head>
<body>
    
    <section class="header_section">
        <div class="container">
            <div class="row">
                <div class="col-5 col-sm-2" style="">
                    <a href="<?php echo site_url(); ?>">
                        <img src="<?php echo site_url(); ?>/assets/frontend/images/logo.png" alt="" class="img-fluid">
                    </a>
                </div>
                <div class="col-7 col-sm-10">
                    <div class="btn-group float-right mt-3" role="group" aria-label="Basic example">
                    <?php if($this->user_model->is_user_login()){ ?>
                          <a href="<?php echo site_url('logout'); ?>" class="btn btn-success custom-success"> <i class="fa fa-sign-out"></i> Logout</a>
                          <a href="<?php echo site_url('users/dashboard'); ?>" class="btn btn-primary custom-primary"> <i class="fa fa-user"></i> Profile </a>
                      <?php } else{ ?>
                          <a href="<?php echo site_url('login'); ?>" class="btn btn-success custom-success"> <i class="fa fa-sign-in"></i> Login</a>
                          
                          <a href="<?php echo site_url('signup'); ?>" class="btn btn-primary custom-primary"> <i class="fa fa-user-plus"></i> Signup</a>
                      <?php } ?>
                      
                      
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="menu_section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <nav class="navbar navbar-expand-lg navbar-dark menu-bg">
                      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                      </button>
                      <div class="collapse navbar-collapse" id="navbarText">
                        <ul class="navbar-nav mr-auto">
                          <li class="nav-item active">
                            <a class="nav-link" href="<?php echo site_url(); ?>"> <i class="fa fa-home"></i> Home</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="<?php echo site_url('property'); ?>"> <i class="fa fa-map-marker"></i>  Property </a>
                          </li>
                          <?php if($this->user_model->is_user_login()){ ?>
                          <li class="nav-item">
                            <a class="nav-link" href="<?php echo site_url('profile/owners'); ?>"><i class="fa fa-user"></i> Property Owners</a>
                          </li>
                          <?php } ?>
                          <li class="nav-item">
                            <a class="nav-link" href="<?php echo site_url('about'); ?>"><i class="fa fa-info"></i> About</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="<?php echo site_url('contact'); ?>"> <i class="fa fa-envelope-o"></i> Contact</a>
                          </li>
                        </ul>
                        <?php if($this->user_model->is_user_login()){ ?>
                        <ul class="navbar-nav my-2 my-lg-0">
                          <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="fa fa-user"></i> Profile
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                              <a class="dropdown-item" href="<?php echo site_url('users/dashboard'); ?>"> <i class="fa fa-home"></i> Dashboard</a>
                              <a class="dropdown-item" href="<?php echo site_url('users/profile'); ?>"> <i class="fa fa-gear"></i> My Profile</a>
                              <a class="dropdown-item" href="<?php echo site_url('users/edit_profile'); ?>"> <i class="fa fa-edit"></i> Edit Profile</a>
                              
                              <?php if($this->session->userdata('current_user_type')=='owner'){ ?>
                              <a class="dropdown-item" href="<?php echo site_url('users/property'); ?>"> <i class="fa fa-map-o"></i> Property</a>
                              <?php } ?>
                              
                              <a class="dropdown-item" href="<?php echo site_url('users/messenger'); ?>"> <i class="fa fa-envelope-o"></i> Messenger <span class="badge badge-danger"><?php echo $this->user_model->my_message_count(); ?></span></a>
                              
                              <a class="dropdown-item" href="<?php echo site_url('logout'); ?>"> <i class="fa fa-power-off"></i> Logout </a>
                            </div>
                          </li>
                        </ul>
                        <?php } ?>
                      </div>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    
    <?php if($this->session->userdata('error_msg') or $this->session->userdata('success_msg')){ ?>
   <section class="alert_message_section" style="padding: 25px 0px;">
       <div class="container">
           <div class="row">
               <div class="col-sm-12">
                   <div class="alert alert-<?php if($this->session->userdata('error_msg')){echo 'danger';}else{echo 'success';} ?> alert-dismissible fade show" role="alert">
                       <?php echo $this->session->userdata('error_msg'); echo $this->session->userdata('success_msg'); ?>
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
               </div>
           </div>
       </div>
   </section>
   <?php $this->session->set_userdata('error_msg', ''); $this->session->set_userdata('success_msg', ''); ?>
   <?php } ?>