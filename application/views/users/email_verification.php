<?php $this->load->view('frontend/header'); ?>
    
    <section class="doctors_list">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 mt-5">
                    <?php $this->load->view('users/sidebar'); ?>
                </div>
                
                
                <div class="col-sm-9 mt-5" style="min-height: 500px;">
                    <h4>Email Verification</h4>
                    <hr>
                    
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>You have to complete your emial verification first.</h4>
                            <h5>If you do not receive an email please plick the button bellow.</h5>
                        </div>
                        <div class="col-sm-12">
                            <?php echo form_open('users/email_verification'); ?>
                            <input name="submit_verification_email" type="submit" value="Submit Verification Email" class="btn btn-info">
                            <?php echo form_close(); ?>
                        </div>
                        
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    
    <?php $this->load->view('frontend/footer'); ?>