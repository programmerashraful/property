<?php $this->load->view('frontend/header'); ?>
    
    <section class="doctors_list">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 mt-5">
                    <?php $this->load->view('users/sidebar'); ?>
                </div>
                
                
                <div class="col-sm-9 mt-5" style="min-height: 500px;">
                    <h4>Welcome to dashboard</h4>
                    <hr>
                    
                    <div class="row">
                        <div class="col-sm-3">
                            <a href="<?php echo site_url('users'); ?>" class="btn btn-default dashboard_icon">
                                <i class="fa fa-home"></i> <br>
                                <span>Dashboard</span>
                            </a>
                        </div>
                        
                        <div class="col-sm-3">
                            <a href="<?php echo site_url('users/profile'); ?>" class="btn btn-default dashboard_icon">
                                <i class="fa fa-gear"></i> <br>
                                <span>My Profile</span>
                            </a>
                        </div>
                        
                        <div class="col-sm-3">
                            <a href="<?php echo site_url('users/edit_profile'); ?>" class="btn btn-default dashboard_icon">
                                <i class="fa fa-edit"></i> <br>
                                <span>Edit Profile</span>
                            </a>
                        </div>
                        
                        <div class="col-sm-3" style="position:relative">
                            <a href="<?php echo site_url('users/messenger'); ?>" class="btn btn-default dashboard_icon">
                                <i class="fa fa-envelope-o"></i> <br>
                                <span>Messenger</span>
                                <span class="badge badge-danger messenger_badge"><?php echo $this->user_model->my_message_count(); ?></span>
                            </a>
                        </div>
                        
                        <div class="col-sm-3">
                            <a href="<?php echo site_url('logout'); ?>" class="btn btn-default dashboard_icon">
                                <i class="fa fa-power-off"></i> <br>
                                <span>Logout</span>
                            </a>
                        </div>
                    
                        
                        <?php if($this->user_type=='owner'){ ?>
                        <div class="col-sm-3">
                            <a href="<?php echo site_url('users/property'); ?>" class="btn btn-default dashboard_icon">
                                <i class="fa  fa-map-o"></i> <br>
                                <span>Property</span>
                            </a>
                        </div>
                        <?php } ?>
                        
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    
    <?php $this->load->view('frontend/footer'); ?>