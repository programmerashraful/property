<?php $this->load->view('frontend/header'); ?>
    
    <section class="doctors_list">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 mt-5">
                    <?php $this->load->view('users/sidebar'); ?>
                </div>
                
                
                <div class="col-sm-9 mt-5" style="min-height: 500px;">
                    <h4>Become Our Member</h4>
                    <hr>
                    
                    <div class="row">
                        <div class="col-sm-12">
                            <h4 style="font-weight:300">Please pay membership fee <b> &#x9f3;<?php echo $this->user_model->get_setting_data('membership_fee'); ?></b> to our bkash number <b>+8801737XXXXXX</b></h4>
                            <h5>After sent the money please fillup the form bellow</h5>
                        </div>
                        <div class="col-sm-12">
                            <hr>
                        </div>
                        <div class="col-sm-12">
                            <?php echo form_open('users/membership'); ?>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                       <label for="bkash_phone">bKash Sender Phone</label>
                                        <input type="text" name="bkash_phone" id="bkash_phone" placeholder="bkash sender phone number" value="<?php echo $user->bkash_phone; ?>" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                       <label for="bkash_trx">bKash TRX ID</label>
                                        <input type="text" name="bkash_trx" id="bkash_trx" placeholder="bKash Transaction ID" value="<?php echo $user->bkash_trx; ?>" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <input name="save_payment_info" type="submit" value="Send Information" class="btn btn-info">
                            <?php echo form_close(); ?>
                        </div>
                        
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    
    <?php $this->load->view('frontend/footer'); ?>