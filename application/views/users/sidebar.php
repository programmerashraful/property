<?php 
$users = $this->user_model->get_user($this->session->userdata('current_user_id'));
if($users){
    
?>
   <div class="single_property">
    <div class="image">
        <img src="<?php echo site_url('uploads/users/'.$users->image); ?>" alt="" class="img-fluid">
    </div>
    <div class="property_footer">
        <div class="info text-center">
            <h4><?php echo $users->full_name; ?></h4>
        </div>
    </div>
</div>
<?php } ?>