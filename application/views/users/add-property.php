<?php $this->load->view('frontend/header'); ?>
<?php echo form_open_multipart('users/add_new_property'); ?>
<section class="property_details">
  
    <section class="property_owner_section">
        <div class="container white_block" >
            <div class="row">
                <div class="col-sm-3">
                    <a href="<?php echo site_url('users/dashboard/'); ?>" class="btn btn-success btn-block"> <i class="fa fa-home"></i> Dashboard</a>
                </div>
                <div class="col-sm-3 ">
                    <a href="<?php echo site_url('users/profile/'); ?>" class="btn btn-success btn-block"> <i class="fa fa-gear"></i> My Profile</a>
                </div>
                <div class="col-sm-3">
                    <a href="<?php echo site_url('users/edit_profile/'); ?>" class="btn btn-success btn-block"> <i class="fa fa-edit"></i> Edit Profile</a>
                </div>
                <div class="col-sm-3">
                    <a href="<?php echo site_url('users/property/'); ?>" class="btn btn-success btn-block"> <i class="fa fa-map-o"></i> Property</a>
                </div>
            </div>
        </div>
    </section>
    
    <section class="property_details pb-3" style="min-height:500px">
        <div class="container" id="app">
           
            <div class="row mb-2" style="color:#28A745">
                <div class="col-sm-12 white_block">
                   <div class="row">
                       <div class="col-sm-6">
                            <label for="title">Property Title</label>
                           <div class="input-group">
                            <div class="input-group-prepend">
                              <div class="input-group-text"><i style="color:#007b46" class="fa fa-tag"></i></div>
                            </div>
                            <input type="text" id="title" class="form-control form-control-lg" name="title" placeholder="Property Title" value="" required>
                          </div>
                       </div>
                       <div class="col-sm-6">
                            <label for="gender">Cateogry</label>
                           <div class="input-group">
                            <div class="input-group-prepend">
                              <div class="input-group-text"><i style="color:#007b46" class="fa fa-tag"></i></div>
                            </div>
                               <select  id="category" class="form-control custom-select-lg custom-select" name="type" placeholder="Category" required>
                                    <option value="Sale">For Sale</option>
                                  <option value="Rent">For Rent</option>
                                  <option value="Rent With Furniture">Rent With Furniture</option>
                                  <option value="Rent For Bachelor">Rent For Bachelor</option>
                               </select>
                          </div>
                       </div>
                   </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-12 white_block">
                    <div class="row mb-2" style="color:#28A745">
                        <div class="col-sm-4">
                            <div class="">
                              <label for="address">Address (Property Location)</label>
                               <div class="input-group">
                                <div class="input-group-prepend">
                                  <div class="input-group-text"><i style="color:#007b46" class="fa fa-location-arrow"></i></div>
                                </div>
                                <input id="address" type="text" class="form-control form-control-lg" name="address" placeholder="Address"  required>
                              </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="">
                                <label for="phone">phone (For Contact)</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                      <div class="input-group-text"><i style="color:#007b46" class="fa fa-phone"></i></div>
                                    </div>
                                    <input type="text" id="phone" class="form-control form-control-lg" name="phone" placeholder="Phone" required>
                                  </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="">
                                <label for="email">E-mail (For Contact)</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                      <div class="input-group-text"><i style="color:#007b46" class="fa fa-envelope-o"></i></div>
                                    </div>
                                    <input type="email" id="email" class="form-control form-control-lg" name="email" placeholder="Email"  required>
                                  </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="row mt-2" style="color:#28A745">
                        <div class="col-sm-6">
                            <label for="division_id">Division</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                  <div class="input-group-text"><i style="color:#007b46" class="fa fa-tag"></i></div>
                                </div>
                                <select name="division_id" class="form-control custom-select custom-select-lg" id="division_id" v-model="division_id" @change="getDistricts()" required>
                                  <option value="">Division..</option>
                                  <option v-for="(division,key,index) in divisions" v-bind:value="division.id"> {{ division.name }} </option>
                                </select>
                              </div>
                        </div>
                        <div class="col-sm-6">
                           
                            <label for="district_id">District</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                  <div class="input-group-text"><i style="color:#007b46" class="fa fa-tag"></i></div>
                                </div>
                                <select name="district_id" class="form-control custom-select custom-select-lg" id="district_id" @change="getMedicals()" v-model="district_id" required>
                                  <option value="">Districts..</option>
                                  <option v-for="(district,key,index) in districts" v-bind:value="district.id"> {{ district.name }} </option>
                                </select>
                              </div>
                        </div>
                        
                    </div>
                    
                    
                    <div class="row mt-2" style="color:#28A745">
                        <div class="col-sm-4">
                           <label for="bed_room">Bed Room</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                  <div class="input-group-text"><i style="color:#007b46" class="fa fa-tag"></i></div>
                                </div>
                                <input type="number" id="bed_room" class="form-control form-control-lg" name="bed_room" placeholder="Bed Room" value="" required>
                              </div>
                        </div>
                        <div class="col-sm-4">
                           
                            <label for="bath_room">Bath Room</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                  <div class="input-group-text"><i style="color:#007b46" class="fa fa-tag"></i></div>
                                </div>
                                <input id="bath_room" type="number" class="form-control form-control-lg" name="bath_room" placeholder="Bath room" value="" required>
                              </div>
                        </div>
                        <div class="col-sm-4">
                           
                            <label for="dining_room">Dining Room</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                  <div class="input-group-text"><i style="color:#007b46" class="fa fa-tag"></i></div>
                                </div>
                                <input id="dining_room" type="text" class="form-control form-control-lg" name="dining_room" placeholder="Dining room" value="" required>
                              </div>
                        </div>
                    </div>
                    
                    
                    <div class="row mt-2" style="color:#28A745">
                        <div class="col-sm-4">
                           <label for="drawing_room">Drawing Room</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                  <div class="input-group-text"><i style="color:#007b46" class="fa fa-tag"></i></div>
                                </div>
                                <input type="number" id="drawing_room" class="form-control form-control-lg" name="drawing_room" placeholder="Drawing Room" value="" required>
                              </div>
                        </div>
                        <div class="col-sm-4">
                           
                            <label for="lift">Lift</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                  <div class="input-group-text"><i style="color:#007b46" class="fa fa-tag"></i></div>
                                </div>
                                <select  id="lift" class="form-control custom-select-lg custom-select" name="lift" placeholder="Lift" required>
                                    <option value="">Lift Availability</option>
                                    <option value="Yes">Yes</option>
                                  <option value="No">No</option>
                               </select>
                              </div>
                        </div>
                        <div class="col-sm-4">
                           
                            <label for="garage">Garage</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                  <div class="input-group-text"><i style="color:#007b46" class="fa fa-tag"></i></div>
                                </div>
                                <select  id="garage" class="form-control custom-select-lg custom-select" name="garage" placeholder="Garage" required>
                                    <option value="">Garage Availability</option>
                                    <option value="Yes">Yes</option>
                                  <option value="No">No</option>
                               </select>
                              </div>
                        </div>
                    </div>
                    
                    
                    <div class="row mt-2" style="color:#28A745">
                        <div class="col-sm-4">
                           <label for="property_image">Property Image (550px X 310px)</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                  <div class="input-group-text"><i style="color:#007b46" class="fa fa-tag"></i></div>
                                </div>
                                <input id="property_image" type="file" class="form-control form-control-lg" name="property_image" placeholder="Property Image" required>
                              </div>
                        </div>
                        <div class="col-sm-4">
                           <label for="price">Property Price</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                  <div class="input-group-text"><i style="color:#007b46" class="fa fa-tag"></i></div>
                                </div>
                                <input id="price" type="number" class="form-control form-control-lg" name="price" placeholder="Property Price" required>
                              </div>
                        </div>
                        <div class="col-sm-4">
                           
                            <label for="rent_type">Rent Type (If property type is rent)</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                  <div class="input-group-text"><i style="color:#007b46" class="fa fa-tag"></i></div>
                                </div>
                                <select  id="rent_type" class="form-control custom-select-lg custom-select" name="rent_type" placeholder="Rent Tipe">
                                    <option value="">Rent Type</option>
                                    <option value="Monthly">Monthly</option>
                                      <option value="Yearly">Yearly</option>
                               </select>
                              </div>
                        </div>
                       
                    </div>
                    
                </div>
            </div>
            
            
            
            <div class="row mt-2" id="app">
                <div class="col-sm-12 white_block">
                    <h4 style="font-weight:400" ><i class="fa fa-photo"></i> Pictures <b></b></h4>
                    <div class="row mb-3">
                        <div class="seperator"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">
                            <div class="property_picture_upload mb-3">
                               <input type="file" name="property_gallery" ref="file_upload"  id="property_gallery" class="property_image" @change="uploadImage">
                               <span>
                                   <i v-bind:class="imageUploading ? 'fa fa-spinner fa-spin' : 'fa fa-camera'"></i>
                               </span>
                            </div>
                        </div>
                        
                        <div class="col-sm-2" v-for="(item,key,index) in property_images">
                            <div class="property_pictures mb-3">
                               <span @click="removePropertyImage(key)"><i class="fa fa-trash"></i></span>
                               <input type="hidden" name="property_images[]" v-bind:value="item">
                                <img v-bind:src="'<?php echo site_url('uploads/property/'); ?>'+item" alt="" class="img-fluid  img-thumbnail" style="width:100%;height: 90px;">
                            </div>
                        </div>
                        
                    </div>
                    <div v-if="imageUploadError" class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                               Image Upload error. Try Another Image.
                               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                              </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row mt-2">
               
                <div class="col-sm-6 white_block">
                    <h4 ><i class="fa fa-map-marker"></i> Google Map Location</h4>
                    <div class="row mb-3">
                        <div class="seperator"></div>
                    </div>
                    <textarea class="form-control" name="google_location" id="" cols="30" rows="10" placeholder="Your property google map location give here for customer direction"></textarea>
                </div>
                
                <div class="col-sm-6 white_block">
                    <h4 ><i class="fa fa-youtube"></i> Youtube Video </h4>
                    <div class="row mb-3">
                        <div class="seperator"></div>
                    </div>
                    <textarea class="form-control" name="youtube_video" id="" cols="30" rows="10" placeholder="Share your property youtube video link for customer attraction."></textarea>
                </div>
                
            </div>
            
            <div class="row mt-2">
                <div class="col-sm-12 white_block">
                    <h4 style="font-weight:400" ><i class="fa fa-file-text"></i> Description <b></b></h4>
                    <textarea rows="8"  class="form-control form-control-lg" name="description" placeholder="Write something about your property" required></textarea>
                </div>
            </div>
            
            <div class="row mt-2">
                <div class="col-sm-12 white_block">
                    <h4 style="font-weight:400" ><i class="fa fa-star"></i> Remark you property out of (5) <b></b></h4>
                    <select  id="rating" class="form-control custom-select-lg custom-select" name="rating" placeholder="rating">
                        <option value="1">* One Star</option>
                        <option value="2">** Two Star</option>
                          <option value="3">*** Three Star</option>
                          <option value="4">**** Four Star</option>
                          <option value="5">***** Five Star</option>
                   </select>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-sm-12 white_block">
                    <button type="submit" name="save_property" value="save_property" class="btn btn-success"> <i class="fa fa-save"></i> Save </button>
                    
                    <a href="<?php echo site_url('users/dashboard/'); ?>" class="btn btn-success float-right"> <i class="fa fa-home"></i> Dashboard</a>
                </div>
            </div>
            
           
            
        </div>
    </section>
    
   
</section>
<?php echo form_close(); ?>
    
    <?php $this->load->view('frontend/footer'); ?>