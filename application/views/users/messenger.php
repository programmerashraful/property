<?php $this->load->view('frontend/header'); ?>
<link rel="stylesheet" href="<?php echo site_url('assets/frontend/css/chatbox.css'); ?>">
    
<section class="property_details">
   <?php if($user){ ?>
    <section class="property_owner_section">
        <div class="container white_block" >
            <div class="row">
                <div class="col-sm-2 " style="border-right:1px solid #DDD">
                    <a href="<?php echo site_url('users/dashboard/'); ?>" class="owner_name_image">
                        <img style="max-width:100px" src="<?php echo site_url('uploads/users/'.$user->image); ?>" alt="" class="img-fluid rounded-circle img-thumbnail">
                        
                    </a>
                </div>
                <div class="col-sm-8 pt-3" style="color: #28a745;">
                    <h4 style="margin:0;padding:0;font-weight: 300;"><i class="fa fa-user"></i> <?php echo $user->full_name; ?></h4>
                    <h4 style="margin:0;padding:0;font-weight: 300;"><i class="fa fa-eye"></i><b> Gender:-</b> <?php echo $user->gender; ?></h4>
                </div>
                <div class="col-sm-2 pt-4">
                    <a href="<?php echo site_url('users'); ?>" class="btn btn-success btn-block"> <i class="fa fa-home"></i> Dashboard</a>
                    
                </div>
            </div>
        </div>
    </section>
    <?php } ?>
    
    <section class="property_details pb-3" style="min-height:500px">
        <div class="container white_block">
            
            <div class="messaging">
              <div class="inbox_msg">
                <div class="inbox_people">
                  <div class="headind_srch">
                    <div class="recent_heading">
                      <h4>Recent</h4>
                    </div>
                    <div class="srch_bar">
                      <div class="stylish-input-group">
                        <input type="text" class="search-bar"  placeholder="Search" >
                        <span class="input-group-addon">
                        <button type="button"> <i class="fa fa-search" aria-hidden="true"></i> </button>
                        </span> </div>
                    </div>
                  </div>
                  <div class="inbox_chat">
                    <?php /*var_dump($messenger_users);*/ if($messenger_users){foreach($messenger_users as $m_user){if($m_user->id!=$this->user_id){ ?>
                    <div class="chat_list <?php if($m_user->id==$with_id){ echo 'active_chat';} ?>">
                      <div class="chat_people" href="">
                          <a href="<?php echo site_url('users/messenger/'.$m_user->id); ?>">
                            <div class="chat_img"> 
                                <img class=" rounded-circle" src="<?php echo site_url('uploads/users/'.$m_user->image); ?>" alt="sunil"> 
                                <!--<span class="badge badge-danger">5</span>-->
                            </div>
                            <div class="chat_ib" style="">
                              <h5> <?php echo $m_user->full_name; ?>  <span class="chat_date"><?php echo date('M d', strtotime($m_user->date)); ?> </span></h5>
                              <p> <?php echo $m_user->message; ?></p>
                            </div>
                          </a>
                      </div>
                    </div>
                    <?php }}} ?>
                    
                  </div>
                </div>
                <div class="mesgs">
                  <div class="msg_history" id="messengerScroll">
                   
                    <?php if($messages){foreach($messages as $message){ ?>
                       
                        <?php if($message->sender_id == $this->user_id){ ?>
                        <div class="outgoing_msg">
                          <div class="sent_msg">
                            <p><?php echo $message->message; ?></p>
                            <span class="time_date"> <?php echo date('d/m/Y,  H:i:s', strtotime($message->date)); ?></span> </div>
                        </div>
                        <?php }else{ ?>
                        <div class="incoming_msg">
                          <div class="incoming_msg_img"> <img style="border-radius:50%" src="<?php echo site_url('uploads/users/'.$user->image); ?>" alt="sunil"> </div>
                          <div class="received_msg">
                            <div class="received_withd_msg">
                              <p><?php echo $message->message; ?></p>
                              <span class="time_date"> <?php echo date('d/m/Y,  H:i:s', strtotime($message->date)); ?></span></div>
                          </div>
                        </div>
                        <?php } ?>
                    <?php }} ?>
                    
                  </div>
                  <?php if($user){ ?>
                  <div class="type_msg">
                    <div class="input_msg_write">
                     <?php echo form_open('users/messenger/'.$user->id); ?>
                        <textarea type="text" name="message" class="form-control write_msg" placeholder="Type a message" required style="border: none;margin-bottom: 5px;max-height: 55px;" ></textarea>
                      <button name="send_message" value="send_message"  class="msg_send_btn" type="submit"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                      <?php echo form_close(); ?>
                    </div>
                  </div>
                  <?php } ?>
                </div>
              </div>
            </div>
           
            
        </div>
    </section>
    
   
</section>
<script>
    var scrolled = false;
    var element = null;
    function updateScroll(){
        if(!scrolled){
            var element = document.getElementById("messengerScroll");
            element.scrollTop = element.scrollHeight;
             scrolled=true;
        }
    }
    setInterval(updateScroll,100);

</script>

    
    <?php $this->load->view('frontend/footer'); ?>