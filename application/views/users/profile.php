<?php $this->load->view('frontend/header'); ?>
    
<section class="property_details">
   <?php if($user){ ?>
    <section class="property_owner_section">
        <div class="container white_block" >
            <div class="row">
                <div class="col-sm-2 " style="border-right:1px solid #DDD">
                    <a href="<?php echo site_url('users/dashboard/'); ?>" class="owner_name_image">
                        <img style="max-width:100px" src="<?php echo site_url('uploads/users/'.$user->image); ?>" alt="" class="img-fluid rounded-circle img-thumbnail">
                        
                    </a>
                </div>
                <div class="col-sm-8">
                   
                </div>
                <div class="col-sm-2 pt-3">
                    <a href="<?php echo site_url('users/edit_profile/'); ?>" class="btn btn-success btn-block"> <i class="fa fa-edit"></i> Edit Profile</a>
                    <a href="<?php echo site_url('users/'); ?>" class="btn btn-success btn-block"> <i class="fa fa-home"></i> Dashboard</a>
                    
                </div>
            </div>
        </div>
    </section>
    <?php } ?>
    
    <section class="property_details pb-3" style="min-height:500px">
        <div class="container">
           
            <div class="row mb-2">
                <div class="col-sm-6 white_block" style="border-radius: 5px 0px 0px 5px; " >
                    <h4 style="margin:0;padding:0"><i class="fa fa-user"></i> <?php echo $user->full_name; ?></h4>
                </div>
                <div class="col-sm-6 white_block" style="border-radius: 0px 5px 5px 0px;">
                    <h4 style="margin:0;padding:0"><i class="fa fa-eye"></i><b> Gender:-</b> <?php echo $user->gender; ?></h4>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-12 white_block">
                    <div class="row mb-2">
                        <div class="col-sm-4" style="border-right: 1px solid #DDD;">
                            <div class="property_block">
                                <i class="fa fa-location-arrow"></i>
                                <a href=""><?php echo $user->address; ?></a>
                            </div>
                        </div>
                        <div class="col-sm-4" style="border-right: 1px solid #DDD;">
                            <div class="property_block">
                                <i class="fa fa-phone"></i>
                                <a href="tel:<?php echo $user->phone; ?>"><?php echo $user->phone; ?></a>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="property_block">
                                <i class="fa fa-envelope-o"></i>
                                <a href="mailto:<?php echo $user->email; ?>"><?php echo $user->email;  ?></a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="seperator">
                            
                        </div>
                    </div>
                    
                    <div class="row mt-2" style="color:#28A745">
                        <div class="col-sm-4">
                            <p><b><i class="fa fa-tag"></i> Country:- </b> <?php echo $user->country; ?></p>
                        </div>
                        <div class="col-sm-4">
                            <p><b><i class="fa fa-tag"></i> City:- </b> <?php echo $user->city; ?></p>
                        </div>
                        <div class="col-sm-4">
                            <p><b><i class="fa fa-tag"></i> State:- </b> <?php echo $user->state; ?></p>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="seperator">
                            
                        </div>
                    </div>
                    
                    <div class="row mt-2" style="color:#28A745">
                        <div class="col-sm-4">
                            <p><b><i class="fa fa-tag"></i> NID Number:- </b> <?php echo $user->nid_number; ?></p>
                        </div>
                        <div class="col-sm-4">
                            <p><b><i class="fa fa-tag"></i> Post Code:- </b> <?php echo $user->post_code; ?></p>
                        </div>
                        <div class="col-sm-4">
                            <p><b><i class="fa fa-tag"></i> Gender:- </b> <?php echo $user->gender; ?></p>
                        </div>
                    </div>
                </div>
            </div>
            
            
            <div class="row mt-2">
                <div class="col-sm-12 white_block">
                    <h4 style="font-weight:400" ><i class="fa fa-file-text"></i> About <b><?php echo $user->full_name; ?></b></h4>
                    <p><?php echo $user->about; ?></p>
                </div>
            </div>
            
           
            
        </div>
    </section>
    
   
</section>

    
    <?php $this->load->view('frontend/footer'); ?>