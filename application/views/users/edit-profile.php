<?php $this->load->view('frontend/header'); ?>
<?php echo form_open_multipart('users/edit_profile'); ?>
<section class="property_details">
   <?php if($user){ ?>
    <section class="property_owner_section">
        <div class="container white_block" >
            <div class="row">
                <div class="col-sm-2 ">
                    <a href="<?php echo site_url('users/dashboard/'); ?>" class="owner_name_image">
                        <img style="" src="<?php echo site_url('uploads/users/'.$user->image); ?>" alt="" class="img-fluid rounded-circle img-thumbnail">
                        
                    </a>
                </div>
                <div class="col-sm-8">
                   
                </div>
                <div class="col-sm-2 pt-3">
                    <a href="<?php echo site_url('users/dashboard/'); ?>" class="btn btn-success btn-block"> <i class="fa fa-home"></i> Dashboard</a>
                    
                    <button type="submit" name="save_profile" value="save_profile" class="btn btn-success btn-block"> <i class="fa fa-save"></i> Save Change</button>
                    
                </div>
            </div>
        </div>
    </section>
    <?php } ?>
    
    <section class="property_details pb-3" style="min-height:500px">
        <div class="container">
           
            <div class="row mb-2" style="color:#28A745">
                <div class="col-sm-12 white_block">
                   <div class="row">
                       <div class="col-sm-6">
                            <label for="full_name">Full name</label>
                           <div class="input-group">
                            <div class="input-group-prepend">
                              <div class="input-group-text"><i style="color:#007b46" class="fa fa-user"></i></div>
                            </div>
                            <input type="text" id="full_name" class="form-control form-control-lg" name="full_name" placeholder="Full Name" value="<?php echo $user->full_name; ?>" required>
                          </div>
                       </div>
                       <div class="col-sm-6">
                            <label for="gender">Gender</label>
                           <div class="input-group">
                            <div class="input-group-prepend">
                              <div class="input-group-text"><i style="color:#007b46" class="fa fa-tag"></i></div>
                            </div>
                               <select  id="gender" class="form-control custom-select-lg custom-select" name="gender" placeholder="Gender" required>
                                   <option value="Male" <?php if($user->gender=='Male'){echo 'selected';}?>>Male</option>
                                   <option value="Female" <?php if($user->gender=='Female'){echo 'selected';}?>>Female</option>
                               </select>
                          </div>
                       </div>
                   </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-12 white_block">
                    <div class="row mb-2" style="color:#28A745">
                        <div class="col-sm-4">
                            <div class="">
                              <label for="address">Address</label>
                               <div class="input-group">
                                <div class="input-group-prepend">
                                  <div class="input-group-text"><i style="color:#007b46" class="fa fa-location-arrow"></i></div>
                                </div>
                                <input id="address" type="text" class="form-control form-control-lg" name="address" placeholder="Address" value="<?php echo $user->address; ?>" required>
                              </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="">
                                <label for="phone">phone</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                      <div class="input-group-text"><i style="color:#007b46" class="fa fa-phone"></i></div>
                                    </div>
                                    <input type="text" id="phone" class="form-control form-control-lg" name="phone" placeholder="Phone" value="<?php echo $user->phone; ?>" required>
                                  </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="">
                                <label for="email">E-mail</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                      <div class="input-group-text"><i style="color:#007b46" class="fa fa-envelope-o"></i></div>
                                    </div>
                                    <input type="email" id="email" class="form-control form-control-lg" name="email" placeholder="Email" value="<?php echo $user->email; ?>" required>
                                  </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="row mt-2" style="color:#28A745">
                        <div class="col-sm-4">
                            <label for="country">Country</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                  <div class="input-group-text"><i style="color:#007b46" class="fa fa-tag"></i></div>
                                </div>
                                <input type="text" id="country" class="form-control form-control-lg" name="country" placeholder="Country" value="<?php echo $user->country; ?>" required>
                              </div>
                        </div>
                        <div class="col-sm-4">
                           
                            <label for="city">City</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                  <div class="input-group-text"><i style="color:#007b46" class="fa fa-tag"></i></div>
                                </div>
                                <input type="text" id="city" class="form-control form-control-lg" name="city" placeholder="City" value="<?php echo $user->city; ?>" required>
                              </div>
                        </div>
                        <div class="col-sm-4">
                           
                            <label for="state">State</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                  <div class="input-group-text"><i style="color:#007b46" class="fa fa-tag"></i></div>
                                </div>
                                <input type="text" id="state" class="form-control form-control-lg" name="state" placeholder="State" value="<?php echo $user->state; ?>" required>
                              </div>
                        </div>
                    </div>
                    
                    
                    <div class="row mt-2" style="color:#28A745">
                        <div class="col-sm-4">
                           <label for="nid_number">NID Number</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                  <div class="input-group-text"><i style="color:#007b46" class="fa fa-tag"></i></div>
                                </div>
                                <input type="text" id="nid_number" class="form-control form-control-lg" name="nid_number" placeholder="NID Number" value="<?php echo $user->nid_number; ?>" required>
                              </div>
                        </div>
                        <div class="col-sm-4">
                           
                            <label for="post_code">Post Code</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                  <div class="input-group-text"><i style="color:#007b46" class="fa fa-tag"></i></div>
                                </div>
                                <input id="post_code" type="text" class="form-control form-control-lg" name="post_code" placeholder="Post Code" value="<?php echo $user->post_code; ?>" required>
                              </div>
                        </div>
                        <div class="col-sm-4" >
                           
                            <label for="profile_picture">Profile Picture</label>
                            <div class="input-group"    data-toggle="tooltip" data-placement="top" title="If you need to change your profile picture, You can select your new profile picture. Otherwise You can leave this box blank.">
                                <div class="input-group-prepend">
                                  <div class="input-group-text"><i style="color:#007b46" class="fa fa-tag"></i></div>
                                </div>
                                <input id="profile_picture" type="file" class="form-control form-control-lg" name="profile_picture" placeholder="Profile Picture">
                              </div>
                        </div>
                    </div>
                    
                    
                    <div class="row mt-2" style="color:#28A745">
                        <div class="col-sm-6">
                           <label for="password">Password</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                  <div class="input-group-text"><i style="color:#007b46" class="fa fa-lock"></i></div>
                                </div>
                                <input type="password" id="password" class="form-control form-control-lg" name="password" placeholder="Password Change"  data-toggle="tooltip" data-placement="top" title="If you need to change your password, You can type your new password. Otherwise You can leave this box blank.">
                              </div>
                        </div>
                        <div class="col-sm-6">
                           
                            <label for="post_code">Username</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                  <div class="input-group-text"><i style="color:#007b46" class="fa fa-tag"></i></div>
                                </div>
                                <input id="username" type="text" class="form-control form-control-lg" name="username" placeholder="Username" value="<?php echo $user->user_name; ?>" disabled>
                              </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
            <div class="row mt-2">
                <div class="col-sm-12 white_block">
                    <h4 style="font-weight:400" ><i class="fa fa-file-text"></i> About <b><?php echo $user->full_name; ?></b></h4>
                    <textarea rows="8"  class="form-control form-control-lg" name="about" placeholder="Write something about your self" required><?php echo $user->about; ?></textarea>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-sm-12 white_block">
                    <button type="submit" name="save_profile" value="save_profile" class="btn btn-success"> <i class="fa fa-save"></i> Save Change</button>
                    
                    <a href="<?php echo site_url('users/dashboard/'); ?>" class="btn btn-success float-right"> <i class="fa fa-home"></i> Dashboard</a>
                </div>
            </div>
            
           
            
        </div>
    </section>
    
   
</section>
<?php echo form_close(); ?>
    
    <?php $this->load->view('frontend/footer'); ?>