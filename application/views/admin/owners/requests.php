<?php $this->load->view('admin/inc/header'); ?>
  <!-- Left side column. contains the logo and sidebar -->
<?php $this->load->view('admin/inc/sidebar'); ?>
 
<?php  $query = http_build_query($_GET); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header  no-print">
      <h1>
     <i class="fa fa-envelope-o"></i> Owners 
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Home </a></li>
        <li class="active"> Owner </li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content" style="padding-top:0px">
     
     
     <!--this is error or success message display message-->
     <div class="row no-print" id="message_section">
		    <!--Display the confirmation message -->
            <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
			<div class="col-sm-12 message_display_class">
                <?php if($this->session->userdata('success_msg')): ?>
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if($this->session->userdata('error_msg')): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if(isset($validation_errors)): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $validation_errors; ?>
				</div>
                <?php endif; ?>
                <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
       $this->session->set_userdata($sesattr); ?>
			</div>
            <?php endif; ?>
		</div>
     <!--this is error or success message display message-->
     
     
      <!-- Small boxes (Stat box) -->
      <div class="row" style="margin-top:20px">
            
      <div class="col-sm-12 no-print">
          <div class="box box-success">
            <div class="box-header with-border" style="min-height:410px">
                
                <div class="col-sm-12 table-responsive">
                    <table class="table table-bordered small_table_padding">
                        <thead>
                            <tr>
                                <th> <i class="fa fa-photo "></i> </th>
                                <th> Name </th>
                                <th> Phone </th>
                                <th> Email </th>
                                <th> Address </th>
                                <th> Bkash Phone </th>
                                <th> Bkash TRX ID </th>
                                <th> Action </th>
                            </tr>
                        </thead>
                        <tbody>
                           <?php if($owners){foreach($owners as $owner){ ?>
                            <tr>
                                <td>
                                    <img style="max-width:30px" src="<?php echo site_url('uploads/users/'.$owner->image); ?>" alt="" class="img-responsive">
                                </td>
                                <td><?php echo $owner->full_name; ?></td>
                                <td><?php echo $owner->phone; ?></td>
                                <td><?php echo $owner->email; ?></td>
                                <td><?php echo $owner->address; ?></td>
                                <td><?php echo $owner->bkash_phone; ?></td>
                                <td><?php echo $owner->bkash_trx; ?></td>
                                <td style="width: 64px;">
                                    <div style="width: 64px;" class="btn-group">
                                       
                                        <a onclick="return confirm('Do you want active this owner ?')" href="<?php echo site_url('admin/requests/active/'.$owner->id); ?>" class="btn btn-success btn-xs"> Active </a>
                                        
                                        <a onclick="return confirm('Do you want to delete this data ?')" href="<?php echo site_url('admin/requests/delete/'.$owner->id); ?>" class="btn btn-danger btn-xs"> <i class="fa fa-trash"></i> </a>
                                    </div>
                                </td>
                            </tr>
                            <?php }} ?>
                        </tbody>
                        
                    </table>
                    <div class="row">
                        <div class="col-xs-6">
                            <?php if(isset($links)){ echo $links;} ?>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
       
       
               

       
      </div>
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>



      

 <?php $this->load->view('admin/inc/footer'); ?>