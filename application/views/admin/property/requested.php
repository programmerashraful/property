<?php $this->load->view('admin/inc/header'); ?>
  <!-- Left side column. contains the logo and sidebar -->
<?php $this->load->view('admin/inc/sidebar'); ?>
 
<?php  $query = http_build_query($_GET); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header  no-print">
      <h1>
     <i class="fa fa-check-square-o"></i> Requested Properties 
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Home </a></li>
        <li class="active"> Property </li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content" style="padding-top:0px">
     
     
     <!--this is error or success message display message-->
     <div class="row no-print" id="message_section">
		    <!--Display the confirmation message -->
            <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
			<div class="col-sm-12 message_display_class">
                <?php if($this->session->userdata('success_msg')): ?>
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if($this->session->userdata('error_msg')): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if(isset($validation_errors)): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $validation_errors; ?>
				</div>
                <?php endif; ?>
                <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
       $this->session->set_userdata($sesattr); ?>
			</div>
            <?php endif; ?>
		</div>
     <!--this is error or success message display message-->
     
     
      <!-- Small boxes (Stat box) -->
      <div class="row" style="margin-top:20px">
            
      <div class="col-sm-12 no-print">
          <div class="box box-success">
            <div class="box-header with-border" style="min-height:410px">
                
                <div class="col-sm-12 table-responsive">
                    <table class="table table-bordered small_table_padding">
                        <thead>
                            <tr>
                                <th> <i class="fa fa-photo"></i> </th>
                                <th> Date </th>
                                <th> Title </th>
                                <th> Email </th>
                                <th> Phone </th>
                                <th> Action </th>
                            </tr>
                        </thead>
                        <tbody>
                           <?php if($properties){foreach($properties as $property){ ?>
                            <tr>
                               <td><img style="max-height:30px" src="<?php echo site_url('uploads/property/'.$property->image); ?>" alt="" class="img-responsive"></td>
                                <td><?php echo date('d/m/Y', strtotime($property->entry_date)); ?></td>
                                <td><?php echo $property->title; ?></td>
                                <td><?php echo $property->email; ?></td>
                                <td><?php echo $property->phone; ?></td>
                                <td>
                                    <div class="btn-group">
                                       
                                        <a onclick="return confirm('Do you want to publish this property ?')" href="<?php echo site_url('admin/property/active/'.$property->id); ?>" class="btn btn-success btn-sm"> <i class="fa fa-check-square-o"></i> </a>
                                        
                                        <a target="_blank" href="<?php echo site_url('property/view'.$property->id); ?>" class="btn btn-info btn-sm"> <i class="fa fa-eye"></i> </a>
                                        
                                    </div>
                                </td>
                            </tr>
                            <?php }} ?>
                        </tbody>
                        
                    </table>
                    <div class="row">
                        <div class="col-xs-6">
                            <?php if(isset($links)){ echo $links;} ?>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
       
       
               

       
      </div>
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>



      

 <?php $this->load->view('admin/inc/footer'); ?>