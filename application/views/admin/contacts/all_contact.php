<?php $this->load->view('admin/inc/header'); ?>
  <!-- Left side column. contains the logo and sidebar -->
<?php $this->load->view('admin/inc/sidebar'); ?>
 
<?php  $query = http_build_query($_GET); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header  no-print">
      <h1>
     <i class="fa fa-envelope-o"></i> Contacts 
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Home </a></li>
        <li class="active"> Contact </li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content" style="padding-top:0px">
     
     
     <!--this is error or success message display message-->
     <div class="row no-print" id="message_section">
		    <!--Display the confirmation message -->
            <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
			<div class="col-sm-12 message_display_class">
                <?php if($this->session->userdata('success_msg')): ?>
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if($this->session->userdata('error_msg')): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if(isset($validation_errors)): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $validation_errors; ?>
				</div>
                <?php endif; ?>
                <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
       $this->session->set_userdata($sesattr); ?>
			</div>
            <?php endif; ?>
		</div>
     <!--this is error or success message display message-->
     
     
      <!-- Small boxes (Stat box) -->
      <div class="row" style="margin-top:20px">
            
      <div class="col-sm-12 no-print">
          <div class="box box-success">
            <div class="box-header with-border" style="min-height:410px">
                
                <div class="col-sm-12 table-responsive">
                    <table class="table table-bordered small_table_padding">
                        <thead>
                            <tr>
                                <th> Date </th>
                                <th> Name </th>
                                <th> Email </th>
                                <th> Subject </th>
                                <th> Message </th>
                                <th> Action </th>
                            </tr>
                        </thead>
                        <tbody>
                           <?php if($contacts){foreach($contacts as $contact){ ?>
                            <tr>
                                <td><?php echo date('d/m/Y', strtotime($contact->date_time)); ?></td>
                                <td><?php echo $contact->name; ?></td>
                                <td><?php echo $contact->email; ?></td>
                                <td><?php echo $contact->subject; ?></td>
                                <td><?php echo $contact->message; ?></td>
                                <td>
                                    <div class="btn-group">
                                        <a onclick="return confirm('Do you want to delete this data ?')" href="<?php echo site_url('admin/contacts/delete/'.$contact->id); ?>" class="btn btn-danger btn-xs"> <i class="fa fa-trash"></i> </a>
                                    </div>
                                </td>
                            </tr>
                            <?php }} ?>
                        </tbody>
                        
                    </table>
                    <div class="row">
                        <div class="col-xs-6">
                            <?php if(isset($links)){ echo $links;} ?>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
       
       
               

       
      </div>
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>



      

 <?php $this->load->view('admin/inc/footer'); ?>