<?php 
$user_data = $this->user_model->get_user($this->session->userdata('current_user_id'));
$user_type = $this->session->userdata('current_user_type');
$total_contact = $this->user_model->contacts_count();
$total_requests= $this->user_model->users_count('owner', 'inactive');
$total_p_requests= $this->user_model->properties_count('Requested');
$total_p_published= $this->user_model->properties_count('Published');
$total_car_booking_count = 0;
$new_reservation_count = 0;
$multi_hotel_reservation_count = 0;
?>
  
<?php 

$template = $this->uri->segment(2);
$submenu = $this->uri->segment(3);
$third_menu = $this->uri->segment(4);
$dashboard = '';
$contacts = '';
$requests = '';
$owners = '';
$members = '';
$property_requests = '';
$all_property = '';


//this variable for users
$users ='';
$all_users ='';
$add_new_user ='';

$medicals ='';
$all_medical ='';
$new_medical ='';


$settings='';
$sms_settings='';
$send_sms='';


if($template=='users'){
  $users='active';   
}
elseif($template=='dashboard'){
  $dashboard='active';   
}
elseif($template=='contacts'){
  $contacts='active';   
}
elseif($template=='requests'){
  $requests='active';   
}
elseif($template=='owners'){
  $owners='active';   
}
elseif($template=='members'){
  $members='active';   
}
elseif($template=='property' and $submenu!='requested'){
  $all_property='active';   
}


/*Sub menu handalling*/

if($submenu=='all_user' or $submenu=='edit_user'){
  $all_users='active';   
}
if($submenu=='add_new'){
  $add_new_user='active';   
}
if($template=='property' and $submenu=='requested'){
  $property_requests='active';   
}




?>
 <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo site_url('uploads/users/'.$user_data->image); ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $user_data->first_name.' '.$user_data->last_name; ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        
        <li class="<?php echo $dashboard; ?> treeview">
          <a href="<?php echo site_url('admin/dashboard'); ?>">
            <i class="fa fa-dashboard text-yellow"></i> <span>Dashboard</span>
          </a>
        </li>
        
        
        
        <li class="<?php echo $property_requests; ?> treeview">
          <a href="<?php echo site_url('admin/property/requested'); ?>">
            <i class="fa fa-refresh text-green"></i> <span> Property Request</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-red"><?php echo $total_p_requests; ?></small>
            </span>
          </a>
        </li>
        
        <li class="<?php echo $all_property; ?> treeview">
          <a href="<?php echo site_url('admin/property'); ?>">
            <i class="fa fa-check-square-o text-yellow"></i> <span>  Property Published</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-red"><?php echo $total_p_published; ?></small>
            </span>
          </a>
        </li>
        
        <li class="<?php echo $contacts; ?> treeview">
          <a href="<?php echo site_url('admin/contacts'); ?>">
            <i class="fa fa-envelope-o text-green"></i> <span> Contacts</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-red"><?php echo $total_contact; ?></small>
            </span>
          </a>
        </li>
        
        <li class="<?php echo $requests; ?> treeview">
          <a href="<?php echo site_url('admin/requests'); ?>">
            <i class="fa fa-download text-aqua"></i> <span> Owner Request</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-red"><?php echo $total_requests; ?></small>
            </span>
          </a>
        </li>
        
        <li class="<?php echo $owners; ?> treeview">
          <a href="<?php echo site_url('admin/owners'); ?>">
            <i class="fa fa-user-secret text-yellow"></i> <span> Owners </span>
          </a>
        </li>
        
        <li class="<?php echo $members; ?> treeview">
          <a href="<?php echo site_url('admin/members'); ?>">
            <i class="fa fa-user text-green"></i> <span> Website members</span>
          </a>
        </li>
        
        <li class="<?php echo $users; ?> treeview">
          <a href="<?php echo site_url('admin/users'); ?>">
            <i class="fa fa-users text-orange"></i> <span> All Users </span>
          </a>
        </li>
        
        
        
        <li class="treeview"><a href="<?php echo site_url('logout'); ?>"><i class="fa fa-power-off text-aqua"></i> <span>Log Out</span></a></li>
        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>